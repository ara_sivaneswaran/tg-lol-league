module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Copy web assets from bower_components to more convenient directories.
        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        cwd: 'bower_components/angular/',
                        src: ['**/*.js'],
                        dest: 'resources/assets/js/angular/'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/angular-sanitize/',
                        src: ['**/*.js'],
                        dest: 'resources/assets/js/angular-sanitize/'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/chart.js/dist/',
                        src: ['**/*.js', '**/*.js.map'],
                        dest: 'resources/assets/js/chart.js/'
                    },
                    {
                        expand: true,
                        cwd: 'bower_components/datatables.net/js/',
                        src: ['**/*.js', '**/*.js.map'],
                        dest: 'resources/assets/js/datatables/'
                    },

                    // Fonts.
                    {
                        expand: true,
                        filter: 'isFile',
                        flatten: true,
                        cwd: 'bower_components/',
                        src: ['font-awesome/fonts/**'],
                        dest: 'public/fonts/font-awesome/'
                    },
                ]
            },
        },

        // Compile SASS files into minified CSS.
        sass: {
            app: {
                options: {
                    outputStyle: 'compressed'
                },
                files: {
                    'public/css/app.css': 'resources/assets/sass/app.scss'
                }
            }
        },

        // Watch these files and notify of changes.
        watch: {
            grunt: {files: ['Gruntfile.js']},

            sass: {
                files: [
                    'resources/assets/sass/*.scss',
                    'resources/assets/sass/*/**.scss'
                ],
                tasks: ['sass']
            },

            scripts: {
                files: [
                    'resources/assets/js/*.js',
                    'resources/assets/js/*/*.js',
                    'resources/assets/js/*/**/***.js',
                ],
                tasks: ['concat', 'uglify']
            }
        },

        // Concat files
        concat: {
            options: {
                separator: ';',
            },
            app: {
                src: [
                    'resources/assets/js/app/app.js',
                    'resources/assets/js/app/controllers/*',
                    'resources/assets/js/app/controllers/*/**',
                    'resources/assets/js/vendor/bootstrap.min.js',
                    'resources/assets/js/plugins.js',
                    'resources/assets/js/main.js',
                    'resources/assets/js/custom.js'
                ],
                dest: 'public/js/app.js',
            },
            vendor: {
                src: [
                    'resources/assets/js/vendor/jquery-1.11.0.min.js',
                    'resources/assets/js/angular/angular.js',
                    'resources/assets/js/angular-sanitize/angular-sanitize.js',
                    'resources/assets/js/vendor/jquery-migrate-1.2.1.min.js',
                    'resources/assets/js/vendor/bootstrap.min.js',
                    'resources/assets/js/vendor/placeholdem.min.js',
                    'resources/assets/js/vendor/hoverIntent.js',
                    'resources/assets/js/vendor/superfish.js',
                    'resources/assets/js/vendor/jquery.actual.min.js',
                    'resources/assets/js/vendor/jquerypp.custom.js',
                    'resources/assets/js/vendor/jquery.elastislide.js',
                    'resources/assets/js/vendor/jquery.flexslider-min.js',
                    'resources/assets/js/vendor/jquery.prettyPhoto.js',
                    'resources/assets/js/vendor/jquery.easing.1.3.js',
                    'resources/assets/js/vendor/jquery.ui.totop.js',
                    'resources/assets/js/vendor/jquery.isotope.min.js',
                    'resources/assets/js/vendor/jquery.easypiechart.min.js',
                    'resources/assets/js/vendor/jflickrfeed.min.js',
                    'resources/assets/js/vendor/jquery.sticky.js',
                    'resources/assets/js/vendor/owl.carousel.min.js',
                    'resources/assets/js/vendor/jquery.nicescroll.min.js',
                    'resources/assets/js/vendor/jquery.fractionslider.min.js',
                    'resources/assets/js/vendor/jquery.scrollTo-min.js',
                    'resources/assets/js/vendor/jquery.localscroll-min.js',
                    'resources/assets/js/vendor/jquery.parallax-1.1.3.js',
                    'resources/assets/js/vendor/jquery.bxslider.min.js',
                    'resources/assets/js/vendor/jquery.funnyText.min.js',
                    'resources/assets/js/select2/select2.js',
                    'resources/assets/js/datatables/jquery.dataTables.js',
                ],
                dest: 'public/js/vendor.js',
            },
            styles: {
                src: [
                    'resources/assets/css/*',
                    'resources/assets/css/*/**'
                ],
                dest: 'public/css/theme.css'
            }
        },

        // Minify files
        uglify: {
            options: {
                mangle: false
            },
            scripts: {
                files: {
                    'public/js/app.min.js': ['public/js/app.js'],
                    'public/js/vendor.min.js': ['public/js/vendor.js']
                }
            }
        }
    });

    // Load externally defined tasks.
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // Establish tasks we can run from the terminal.
    grunt.registerTask('build', ['sass', 'copy', 'concat', 'uglify']);
    grunt.registerTask('default', ['build', 'watch']);
}