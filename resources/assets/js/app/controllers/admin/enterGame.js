app.controller('EnterGame', function ($scope) {
    $scope.finished = window.game.finished;
    $scope.newBan = false;

    var init = function() {
        //$('.gameResults').hide();
    };

    $scope.$watch('finished', function(newValue){
        if(newValue){
            $('.gameResults').slideDown();
        }
        else {
            $('.gameResults').slideUp();
        }
    });

    $scope.$watch('newBan', function (newValue) {
        if (newValue) {

        }
        else {

        }
    });

    init();
});