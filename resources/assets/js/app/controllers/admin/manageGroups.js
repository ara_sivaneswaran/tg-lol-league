app.controller('ManageGroups', function ($scope) {
    $scope.groups = window.groups;
    $scope.groupsList = window.groupsList;
    $scope.teamsList = window.teamsList;
    $scope.teams = [];
    $scope.divSize = 'col-md-12';
    $scope.newGroup = null;
    $scope.newTeam = [];

    var init = function () {
        if ($scope.groups.length) {
            var groupsSize = $scope.groups.length;
            var gridSize = 12;

            var division = gridSize / groupsSize;
            $scope.divSize = 'col-md-' + division.toFixed();
        }

        initSelectModel();
    };

    var initSelectModel = function () {
        $scope.newGroup = $scope.groupsList[0];
    }

    $scope.addGroup = function (e) {
        e.preventDefault();

        $scope.groups.push(
            {
                id: $scope.newGroup.id,
                name: $scope.newGroup.name,
                teams: []
            }
        );

        init();
    };

    $scope.addTeam = function (e, groupId) {
        e.preventDefault();

        var team = $scope.newTeam[groupId];

        $scope.groups.forEach(function(group) {
           if(group.id == groupId) {
               group.teams.push(team);
           }
        });
    };

    $scope.removeTeam = function(e, groupId, index) {
        e.preventDefault();

        $scope.groups.forEach(function (group) {
          if(group.id == groupId) {
              group.teams.splice(index, 1);
          }
        });
    }

    init();
});