<section id="abovecontent" class="dark_section bg_image">
	<div class="container">
		<div class="row">
			<div class="block col-sm-12">
				<ul class="breadcrumb">
					<li><a href="/" class="pathway"><i class="fa fa-home"></i></a></li>
					{{ $slot }}
				</ul>
			</div>
		</div>
	</div>
</section>