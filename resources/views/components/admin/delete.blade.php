@include('components.admin.forms.header', ['title' =>  '<strong>Delete</strong> ' . $name])

{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->delete()->action($actionRoute) !!}
{!! BootForm::bind($object) !!}

<p class="text-center">Are you sure you want to delete <span class="text-primary">{{ $name }}</span></p>

@include('components.admin.forms.footer', ['route' =>  $indexRoute, 'submitText' => 'Delete'])
{!! BootForm::close() !!}