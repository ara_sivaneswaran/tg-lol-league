<div class="form-group">
	<div class="col-sm-offset-2 col-sm-10">
		<a href="{{ $route }}" class="btn btn-default btn-lg">
			<i class="fa fa-level-up fa-rotate-270"></i>
		</a>
		<button type="submit" class="btn btn-primary btn-lg">
			<i class="fa fa-save"></i> {{ $submitText }}
		</button>
	</div>
</div>