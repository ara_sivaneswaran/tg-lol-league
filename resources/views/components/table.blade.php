<div class="table-responsive">
	<table class="table table-striped table-hover indexDataTables">
		<thead>
		<tr>
			{{ $table_header }}
		</tr>
		</thead>
		<tbody>
			{{ $table_body }}
		</tbody>
	</table>
</div>