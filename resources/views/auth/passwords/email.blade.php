@extends('layouts.site')

@section('content')
	@component('components.breadcrumbs')
	<li><span>Reset password</span></li>
	@endcomponent
	<section id="portfolio" class="darkgrey_section last_content_section">
		<div class="container">
			<div class="row">
				<section class="col-md-8 col-md-offset-2">
					<div class="classes light_section">
						<div class="media-body">
							<h3><a href="#">Reset password</a></h3>
							<div class="classes-description">
								@if (session('status'))
									<div class="alert alert-success">
										{{ session('status') }}
									</div>
								@endif
								
								<form class="form-horizontal" role="form" method="POST"
								      action="{{ route('password.email') }}">
									{{ csrf_field() }}
									
									<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
										<label for="email" class="col-md-4 control-label">E-Mail Address</label>
										
										<div class="col-md-6">
											<input id="email" type="email" class="form-control" name="email"
											       value="{{ old('email') }}" required>
											
											@if ($errors->has('email'))
												<span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
											@endif
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-md-6 col-md-offset-4">
											<button type="submit" class="btn btn-primary">
												Send Password Reset Link
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
@endsection
