<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">

	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>@if(isset($pageTitle)) {{ $pageTitle }} - @endif{{ config('app.name') }}</title>
	
	<link rel="stylesheet" href="/css/theme.css">
	<link rel="stylesheet" href="/css/app.css">
</head>
<body @if(Route::currentRouteName() == 'home')class="frontpage"@endif>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	your browser</a> to improve your experience.</p>
<![endif]-->

<header id="header">
	<div class="container">
		<div class="row">
			
			<a class="navbar-brand" href="/">TG <span class="highlight">LoL</span> League</a>
			
			<div class="col-sm-12 mainmenu_wrap">
				<div class="main-menu-icon visible-xs"><span></span><span></span><span></span></div>
				<ul id="mainmenu" class="menu sf-menu responsive-menu superfish">
					<li class="active">
						<a href="/">Home</a>
					</li>
					<li class="dropdown">
						<a href="{{ route('schedule') }}">Schedule</a>
					</li>
					<li class="">
						<a href="{{ route('standings') }}">Standings</a>
					</li>
					<li class="dropdown">
						<a href="{{ route('players') }}">Player Stats</a>
					</li>
					<li class="dropdown">
						<a href="{{ route('teams') }}">Teams</a>
					</li>
				</ul>
			</div>
		
		</div>
	</div>
</header>

@yield('content')

<footer id="footer" class="dark_section">
	<div class="container">
		<div class="row">
			<div class="block widget_schedule col-md-3 col-sm-4">
				<h3>Game hours</h3>
				<dl class="dl-horizontal">
					<dt>Sunday</dt>
					<dd><strong>20:00 - 22:00</strong></dd>
					<dt>Wednesday</dt>
					<dd><strong>20:00 - 22:00</strong></dd>
				</dl>
			</div>
			
			
			<div class="block widget_text col-md-6 col-sm-6">
				<h3>More infos</h3>
				<p>Visit our website at: <a href="http://tacticalgaming.net/hq/" target="_blank">tacticalgaming.net/hq/</a></p>
				<p>Our twitch channel: <a href="https://www.twitch.tv/tactical_gaming2004" target="_blank">twitch.tv/tactical_gaming2004</a></p>
			</div>
			
		
		</div>
	</div>
</footer>


<section id="copyright" class="light_section">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				Copyright - <a href="http://arasiva.com" target="_blank">Ara Sivaneswaran</a>
			</div>
		</div>
	</div>
</section>


<div class="preloader">
	<div class="preloaderimg"></div>
</div>

<script src="/js/vendor.min.js"></script>
<script src="/js/app.min.js"></script>
@yield('inline-scripts')
</body>
</html>
