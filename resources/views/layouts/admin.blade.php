@extends('layouts.site')

@section('content')
	@component('components.breadcrumbs')
	@yield('admin_breadcrumb')
	@endcomponent
	<section id="portfolio" class="darkgrey_section last_content_section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<aside class="col-sm-3">
						<div class="block widget_categories">
							<h3>Navigation</h3>
							<ul>
								<li class="cat-item"><a href="#">Dashboard</a></li>
								<li class="cat-item"><a href="{{ route('admin.group.index') }}">Manage groups</a></li>
								<li class="cat-item"><a href="{{ route('admin.match.index') }}">Manage matches</a></li>
								<li class="cat-item"><a href="{{ route('admin.player.index') }}">Manage players</a></li>
								<li class="cat-item"><a href="{{ route('admin.season.index') }}">Manage seasons</a></li>
								<li class="cat-item"><a href="{{ route('admin.team.index') }}">Manage teams</a></li>
								<li class="cat-item"><a href="{{ route('admin.user.index') }}">Manage users</a></li>
								<li class="cat-item">
									<a href="{{ route('logout') }}"
									   onclick="event.preventDefault();
	                                                     document.getElementById('logout-form').submit();">
										Logout
									</a>
									
									<form id="logout-form" action="{{ route('logout') }}" method="POST"
									      style="display: none;">
										{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>
					</aside>
					<div class="col-sm-9">
						@yield('admin_content')
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
