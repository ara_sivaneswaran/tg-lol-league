@extends('layouts.site')

@section('content')
	@component('components.breadcrumbs')
		@yield('breadcrumbs')
	@endcomponent
	
	@yield('main_content')
@endsection
