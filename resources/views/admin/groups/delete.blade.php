@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.group.index') }}">Manage groups</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $group, 'name' => $group->name, 'actionRoute' => route('admin.group.update', ['group' => $group->id]), 'indexRoute' => route('admin.group.index')])
@endsection