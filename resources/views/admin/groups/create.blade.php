@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.group.index') }}">Manage groups</a></span></li>
	<li><span>Create group</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Group'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.group.store')) !!}
	
		{!! BootForm::text('Name', 'name')->placeholder('Name')->required() !!}
		
		@include('components.admin.forms.footer', ['route' =>  route('admin.group.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection