@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.group.index') }}">Manage groups</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> ' . $group->name])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->put()->action(route('admin.group.update', ['group' => $group->id])) !!}
	{!! BootForm::bind($group) !!}
	{!! BootForm::text('Name', 'name')->placeholder('Name')->required() !!}
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.group.index'), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
@endsection