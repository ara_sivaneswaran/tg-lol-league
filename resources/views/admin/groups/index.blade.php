@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span>Manage groups</span></li>
@endsection

@section('admin_content')
	<a href="{{ route('admin.group.create') }}" class="theme_btn pull-right"><i class="fa fa-plus"></i>  Create new group</a>
	<div class="spacer-20"></div>
	
	@include('flash::message')
	
	<div class="spacer-20"></div>
	
	@component('components.table')
		@slot('table_header')
			<th style="width: 80%">Name</th>
			<th></th>
		@endslot
	
		@slot('table_body')
			@foreach($list as $entry)
				<tr>
					<td>{{ $entry->name }}</td>
					<td>
						<a href="{{ route('admin.group.edit', ['group' => $entry->id]) }}" class="control-options primary"><i class="fa fa-pencil"></i></a>
						<a href="{{ route('admin.group.show', ['group' => $entry->id]) }}" class="control-options danger"><i class="fa fa-times"></i></a>
					</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('.indexDataTables').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"columns": [
					{"width": "80%", "targets": 0},
					{"searchable": false, "targets": 1}
				]
			});
		})
	</script>
@endsection