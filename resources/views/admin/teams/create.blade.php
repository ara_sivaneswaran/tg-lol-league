@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.team.index') }}">Manage teams</a></span></li>
	<li><span>Create team</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Team'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.team.store'))->multipart() !!}
	
	@include('admin.teams._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.team.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection