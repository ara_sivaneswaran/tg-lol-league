{!! BootForm::text('Name', 'name')->placeholder('Name')->required() !!}
{!! BootForm::file('Logo', 'logo') !!}
@if(isset($team))
<div class="form-group"><label class="col-sm-4 col-lg-2 control-label" for="logo">Current logo</label>
	<div class="col-sm-8 col-lg-10">
		<img src="{{ route('img', ['path' => $team->present()->imageUrl . '?w=200']) }}" alt="{{ $team->name }}"/>
	</div>
</div>
@endif
{!! BootForm::textarea('Description', 'description')->placeholder('Description')->required() !!}