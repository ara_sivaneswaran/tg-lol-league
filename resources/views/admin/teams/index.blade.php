@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span>Manage teams</span></li>
@endsection

@section('admin_content')
	<a href="{{ route('admin.team.create') }}" class="theme_btn pull-right"><i class="fa fa-plus"></i> Create new
		team</a>
	<div class="spacer-20"></div>
	
	@include('flash::message')
	
	<div class="spacer-20"></div>
	
	@component('components.table')
		@slot('table_header')
			<th style="width: 80%">Team</th>
			<th></th>
		@endslot
		
		@slot('table_body')
			@foreach($list as $entry)
				<tr class="text-center">
					<td>{{ $entry->name }}</td>
					<td>
						<a href="{{ route('admin.team.edit', ['team' => $entry->id]) }}" class="control-options primary"><i
								class="fa fa-pencil"></i></a>
						<a href="{{ route('admin.team.show', ['team' => $entry->id]) }}" class="control-options danger"><i
								class="fa fa-times"></i></a>
					</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('.indexDataTables').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"columns": [
					{"width": "80%", "targets": 0},
					{"searchable": false, "targets": 1}
				]
			});
		})
	</script>
@endsection