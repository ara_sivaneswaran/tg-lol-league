@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.team.index') }}">Manage teams</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $team, 'name' => $team->name, 'actionRoute' => route('admin.team.update', ['team' => $team->id]), 'indexRoute' => route('admin.team.index')])
@endsection