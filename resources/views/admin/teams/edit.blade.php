@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.team.index') }}">Manage teams</a></span></li>
	<li><span>Edit team</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> Team'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->put()->action(route('admin.team.update', ['team' => $team->id]))->multipart() !!}
	{!! BootForm::bind($team) !!}
	
	@include('admin.teams._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.team.index'), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
	
	<hr/>
	
	<h3 class="text-center">Team roster</h3>
	
	<div class="table-responsive">
		<table class="table table-striped text-center">
			<thead>
			<tr>
				<th>Player</th>
				<th>Role</th>
			</tr>
			</thead>
			<tbody>
			@foreach($roster as $player)
				<tr>
					<td>{!! $player->present()->url !!}</td>
					<td><strong>{{ $player->present()->primaryRole }}</strong>/
						{{ $player->present()->secondaryRole }}
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
@endsection