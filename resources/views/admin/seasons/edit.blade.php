@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.season.index') }}">Manage seasons</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> ' . $season->name])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->put()->action(route('admin.season.update', ['season' => $season->id])) !!}
	{!! BootForm::bind($season) !!}
	
	@include('admin.seasons._form')
	
	<hr/>
	<h3>Manage groups and teams</h3>
	
	@include('admin.seasons._manage_groups')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.season.index'), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
@endsection