<div class="form-group" ng-app="TGLoL" ng-controller="ManageGroups">
	<div class="col-sm-offset-2 col-sm-10">
		<select class="form-control" ng-options="group as group.name for group in groupsList track by group.id" ng-model="newGroup">
		
		</select>
		<div class="spacer-10"></div>
		
		<button class="btn btn-lg btn-success" ng-click="addGroup($event)">Add Group</button>
	</div>
	
	<div class="spacer-20"></div>
	
	<div class="col-sm-offset-2 col-sm-10 groups-table">
		<div class="text-center" ng-class="divSize" ng-if="groups.length > 0"
		     ng-repeat="group in groups track by $index">
			
			<div class="groups-name">
				<% group.name %>
			</div>
			<div class="teams">
				<div class="row team" ng-repeat="team in group.teams track by $index">
					<div class="col-md-10"><% team.name %></div>
					<div class="col-md-1 fake-button-container">
						<div class="fake-btn text-danger" ng-click="removeTeam($event, group.id, $index)"><i
								class="fa fa-times"></i></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-10">
						<select class="form-control"
						        ng-options="team as team.name for team in teamsList track by team.id"
						        ng-model="newTeam[group.id]">
						</select>
					</div>
					<div class="col-md-1 fake-button-container">
						<div class="fake-btn text-success" ng-click="addTeam($event, group.id)"><i class="fa fa-check"></i></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
	</div>
	
	<input type="hidden" name="groups" value="<% groups %>" />
</div>