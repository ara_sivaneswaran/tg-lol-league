@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span>Manage seasons</span></li>
@endsection

@section('admin_content')
	<a href="{{ route('admin.season.create') }}" class="theme_btn pull-right"><i class="fa fa-plus"></i> Create new season</a>
	<div class="spacer-20"></div>
	
	@include('flash::message')
	
	<div class="spacer-20"></div>
	
	@component('components.table')
		@slot('table_header')
			<th style="width: 50%">Name</th>
			<th style="width: 20%">Start date</th>
			<th style="width: 20%">End date</th>
			<th></th>
		@endslot
		
		@slot('table_body')
			@foreach($list as $entry)
				<tr class="text-center">
					<td>{{ $entry->name }}</td>
					<td>{{ $entry->present()->startDate }}</td>
					<td>{{ $entry->present()->endDate }}</td>
					<td>
						<a href="{{ route('admin.season.edit', ['season' => $entry->id]) }}" class="control-options primary"><i
								class="fa fa-pencil"></i></a>
						<a href="{{ route('admin.season.show', ['season' => $entry->id]) }}" class="control-options danger"><i
								class="fa fa-times"></i></a>
					</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('.indexDataTables').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"columns": [
					{"width": "45%", "targets": 0},
					{"width": "20%", "targets": 1},
					{"width": "20%", "targets": 2},
					{"searchable": false, "targets": 3}
				]
			});
		})
	</script>
@endsection