@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.season.index') }}">Manage seasons</a></span></li>
	<li><span>Create season</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Group'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.season.store')) !!}
	
	@include('admin.seasons._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.season.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection