@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.season.index') }}">Manage seasons</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $season, 'name' => $season->name, 'actionRoute' => route('admin.season.update', ['season' => $season->id]), 'indexRoute' => route('admin.season.index')])
@endsection