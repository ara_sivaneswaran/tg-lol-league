@inject('service', 'App\Services\FormService')

<div class="form-group">
	<label class="col-sm-4 col-lg-3 control-label" for="match_number">Match number</label>
	<div class="col-sm-8 col-lg-9">
		<input type="number" name="match_number" id="match_number" class="form-control" required="required" @if(isset($game))value="{{ $game->match_number }}"@endif>
	</div>
</div>

{!! BootForm::text('Match history link', 'match_history_link') !!}
{!! BootForm::text('Video link', 'video_link') !!}

@if(isset($game))
	@if($game->finished)
		{!! BootForm::select('Blue Team', 'blue_team_id')->options($service->teamsFromMatchArray($game->match_id))->required()->disabled() !!}
		{!! BootForm::select('Red Team', 'red_team_id')->options($service->teamsFromMatchArray($game->match_id))->required()->disabled() !!}
	@else
		{!! BootForm::select('Blue Team', 'blue_team_id')->options($service->teamsFromMatchArray($game->match_id))->required() !!}
		{!! BootForm::select('Red Team', 'red_team_id')->options($service->teamsFromMatchArray($game->match_id))->required() !!}
	@endif
	<input type="hidden" name="match_id" value="{{ $game->match_id }}"/>
@else
	{!! BootForm::select('Blue Team', 'blue_team_id')->options($service->teamsFromMatchArray($match->id))->required() !!}
	{!! BootForm::select('Red Team', 'red_team_id')->options($service->teamsFromMatchArray($match->id))->required() !!}
	<input type="hidden" name="match_id" value="{{ $match->id }}"/>
@endif