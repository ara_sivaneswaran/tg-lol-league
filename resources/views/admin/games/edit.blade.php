@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matches</a></span></li>
	<li><span><a
				href="{{ route('admin.match.edit', ['match' => $game->match_id]) }}">Match #{{ $game->match_id }}</a></span>
	</li>
	<li><span>Edit game</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> Game'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->put()->action(route('admin.game.update', ['game' => $game->id]))->multipart() !!}
	{!! BootForm::bind($game) !!}
	
	@include('admin.games._form')
	
	@inject('service', 'App\Services\FormService')
	
	<div ng-app="TGLoL" ng-controller="EnterGame">
		{!! BootForm::checkbox('Finished', 'finished')->attribute('ng-model', 'finished') !!}
		
		<div class="gameResults">
			{!! BootForm::select('Winner', 'winner')->options($service->teamsFromMatchArray($game->match_id))->required() !!}
			
			<div class="row text-center">
				<div class="col-md-6">
					<h3 class="text-information">Blue Team</h3>
					
					@if($gameStats)
						@include('admin.games._ban_select_with_data', ['color' => 'blue', 'ban' => $gameStats['blue']['bans']])
					@else
						@include('admin.games._ban_select', ['color' => 'blue'])
					@endif
				</div>
				<div class="col-md-6">
					<h3 class="text-danger">Red Team</h3>
					
					@if($gameStats)
						@include('admin.games._ban_select_with_data', ['color' => 'red', 'ban' => $gameStats['red']['bans']])
					@else
						@include('admin.games._ban_select', ['color' => 'red'])
					@endif
				</div>
			</div>
			
			<div class="row">
				<h3 class="text-information">Blue Team</h3>
				
				
				@if(count($playerStats) > 0)
					@include('admin.games._roster_table_with_data', ['var' => 'blue_team', 'color' => 'blue', 'roster' => $blueRoster, 'playerStats' => $playerStats])
				@else
					@include('admin.games._roster_table', ['var' => 'blue_team', 'color' => 'blue', 'roster' => $blueRoster])
				@endif
			</div>
			<div class="row">
				<h3 class="text-danger">Red Team</h3>
				
				@if(count($playerStats) > 0)
					@include('admin.games._roster_table_with_data', ['var' => 'red_team', 'color' => 'red', 'roster' => $redRoster, 'playerStats' => $playerStats])
				@else
					@include('admin.games._roster_table', ['var' => 'red_team', 'color' => 'red', 'roster' => $redRoster])
				@endif
			</div>
		</div>
	</div>
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.match.edit', ['match' => $game->match_id]), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
@endsection
