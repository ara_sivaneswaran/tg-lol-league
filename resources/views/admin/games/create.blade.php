@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matches</a></span></li>
	<li><span><a href="{{ route('admin.match.edit', ['match' => $match->id]) }}">Match #{{ $match->id }}</a></span></li>
	<li><span>Create game</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Game'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [3, 9]])->post()->action(route('admin.game.store')) !!}
	
	@include('admin.games._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.match.edit', ['match' => $match->id]), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection