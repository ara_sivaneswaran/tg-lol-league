<div class="table-responsive">
	<table class="table @if($color === 'red') red-header @endif">
		<thead>
		<tr>
			<th>Pick order</th>
			<th>Player</th>
			<th>Role</th>
			<th>Champion</th>
			<th>Kills</th>
			<th>Deaths</th>
			<th>Assists</th>
			<th>Damage</th>
		</tr>
		</thead>
		<tbody>
		@for($i = 0; $i < 5; $i++)
			<tr>
				<td style="width: 9%;">
					<input type="number" class="form-control" name="{{ $var }}[{{ $i }}][pick_order]"
					       value="{{ $playerStats[$color][$i]['pick_order'] }}"/>
				</td>
				<td style="width: 20%;">
					<select class="form-control" name="{{ $var }}[{{ $i }}][player_id]">
						@foreach($roster as $player)
							<option value="{{ $player->id }}"
							        @if($playerStats[$color][$i]['player_id'] == $player->id) selected @endif>{{ $player->summoner_name }}</option>
						@endforeach
					</select>
				</td>
				<td style="width: 10%;">
					<select class="form-control" name="{{ $var }}[{{ $i }}][role]">
						@foreach($service->rolesArray() as $id => $role)
							<option value="{{ $id }}"
							        @if($playerStats[$color][$i]['role'] === $id) selected @endif>{{ $role }}</option>
						@endforeach
					</select>
				</td>
				<td style="width: 20%;">
					<select class="form-control" name="{{ $var }}[{{ $i }}][champion]">
						@foreach($service->championsArray() as $id => $champion)
							<option value="{{ $id }}"
							        @if($playerStats[$color][$i]['champion'] == $id) selected @endif>{{ $champion }}</option>
						@endforeach
					</select>
				</td>
				<td style="width: 9%;">
					<input type="number" class="form-control" name="{{ $var }}[{{ $i }}][kills]"
					       value="{{ $playerStats[$color][$i]['kills'] }}"/>
				</td>
				<td style="width: 9%;">
					<input type="number" class="form-control" name="{{ $var }}[{{ $i }}][deaths]"
					       value="{{ $playerStats[$color][$i]['deaths'] }}"/>
				</td>
				<td style="width: 9%;">
					<input type="number" class="form-control" name="{{ $var }}[{{ $i }}][assists]"
					       value="{{ $playerStats[$color][$i]['assists'] }}"/>
				</td>
				<td style="width: 7%;">
					<input type="text" class="form-control" name="{{ $var }}[{{ $i }}][damage]"
					       value="{{ $playerStats[$color][$i]['damage'] }}"/>
				</td>
			</tr>
		@endfor
		</tbody>
	</table>
</div>