@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matches</a></span></li>
	<li><span><a
				href="{{ route('admin.match.edit', ['match' => $game->match_id]) }}">Match #{{ $game->match_id }}</a></span>
	</li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $game, 'name' => 'Game #' .$game->id, 'actionRoute' => route('admin.game.update', ['game' => $game->id]), 'indexRoute' => route('admin.match.edit', ['matchId' => $game->match_id])])
@endsection