@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.user.index') }}">Manage users</a></span></li>
	<li><span>Create user</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> user'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.user.store'))!!}
	
	@include('admin.users._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.user.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection