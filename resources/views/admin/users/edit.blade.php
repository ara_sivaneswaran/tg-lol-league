@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.user.index') }}">Manage users</a></span></li>
	<li><span>Edit user</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> user'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->put()->action(route('admin.user.update', ['user' => $user->id]))->multipart() !!}
	{!! BootForm::bind($user) !!}
	
	@include('admin.users._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.user.index'), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
@endsection