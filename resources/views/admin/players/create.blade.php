@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.player.index') }}">Manage players</a></span></li>
	<li><span>Create player</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Team'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.player.store'))->multipart() !!}
	
	@include('admin.players._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.player.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection