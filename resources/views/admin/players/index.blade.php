@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span>Manage players</span></li>
@endsection

@section('admin_content')
	<a href="{{ route('admin.player.create') }}" class="theme_btn pull-right"><i class="fa fa-plus"></i> Create new
		player</a>
	<div class="spacer-20"></div>
	
	@include('flash::message')
	
	<div class="spacer-20"></div>
	
	@component('components.table')
		@slot('table_header')
			<th style="width: 30%">Summoner name</th>
			<th style="width: 30%">Squad</th>
			<th style="width: 30%">Roles</th>
		<th></th>
		@endslot
		
		@slot('table_body')
			@foreach($list as $entry)
				<tr class="text-center">
					<td>{{ $entry->summoner_name }}</td>
					<td>{{ $entry->present()->teamName }}</td>
					<td>{!! $entry->present()->roles !!}</td>
					<td>
						<a href="{{ route('admin.player.edit', ['player' => $entry->id]) }}" class="control-options primary"><i
								class="fa fa-pencil"></i></a>
						<a href="{{ route('admin.player.show', ['player' => $entry->id]) }}" class="control-options danger"><i
								class="fa fa-times"></i></a>
					</td>
				</tr>
			@endforeach
		@endslot
	@endcomponent
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('.indexDataTables').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"columns": [
					{"width": "30%", "targets": 0},
					{"width": "25%", "targets": 1},
					{"width": "30%", "targets": 2},
					{"searchable": false, "targets": 3}
				]
			});
		})
	</script>
@endsection