@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.player.index') }}">Manage players</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $player, 'name' => $player->present()->full_name, 'actionRoute' => route('admin.player.update', ['player' => $player->id]), 'indexRoute' => route('admin.player.index')])
@endsection