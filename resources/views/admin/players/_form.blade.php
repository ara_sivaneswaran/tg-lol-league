@inject('service', 'App\Services\FormService')

{!! BootForm::text('First name', 'first_name') !!}
{!! BootForm::text('Last name', 'last_name') !!}
{!! BootForm::text('Summoner name', 'summoner_name')->required() !!}
{!! BootForm::select('Primary role', 'primary_role')->options($service->rolesArray())->required() !!}
{!! BootForm::select('Secondary role', 'secondary_role')->options($service->rolesArray())->required() !!}
{!! BootForm::select('Team', 'team_id')->options([null => 'No team'] + $service->teamsArray()) !!}
{!! BootForm::file('Photo', 'photo') !!}
@if(isset($player))
	<div class="form-group"><label class="col-sm-4 col-lg-2 control-label" for="photo">Current photo</label>
		<div class="col-sm-8 col-lg-10">
			<img src="{{ route('img', ['path' => $player->present()->photoUrl . '?w=200']) }}" alt="{{  $player->present()->full_name }}"/>
		</div>
	</div>
@endif