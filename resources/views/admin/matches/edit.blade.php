@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matches</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Edit</strong> ' . $match->name])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->put()->action(route('admin.match.update', ['match' => $match->id])) !!}
	{!! BootForm::bind($match) !!}
	
	@include('admin.matches._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.match.index'), 'submitText' => 'Edit'])
	{!! BootForm::close() !!}
	
	<hr/>
	<div class="spacer-10"></div>
	
	<h3>Manage games</h3>
	<div class="spacer-20"></div>
	
	<a href="{{ route('admin.match.game.get', ['match' => $match->id]) }}" class="pull-right btn btn-success btn-lg white-text"><i class="fa fa-plus"></i> Add game</a>
	<div class="spacer-10"></div>
	
	<div class="responsive-table">
		<table class="table table-striped">
			<thead>
				<tr>
					<th style="width: 15%;">Game #</th>
					<th style="width: 33%;">Red Team</th>
					<th style="width: 33%;">Blue Team</th>
					<th style="width: 5%;">Finished</th>
					<th style="width: 10%;"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($games as $game)
					<tr>
						<td>{{ $game->match_number }}</td>
						<td>{{ $game->red_team->name }}</td>
						<td>{{ $game->blue_team->name }}</td>
						<td>{{ $game->finished }}</td>
						<td>
							<a href="{{ route('admin.game.edit', ['game' => $game->id]) }}"
						       class="control-options primary"><i class="fa fa-pencil"></i></a>
							<a href="{{ route('admin.game.show', ['game' => $game->id]) }}"
							   class="control-options danger"><i class="fa fa-times"></i></a>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection