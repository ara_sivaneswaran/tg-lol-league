@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span>Manage matches</span></li>
@endsection

@section('admin_content')
	<a href="{{ route('admin.match.create') }}" class="theme_btn pull-right"><i class="fa fa-plus"></i> Create new match</a>
	<div class="spacer-20"></div>
	
	@include('flash::message')
	
	<div class="spacer-20"></div>
	
	@component('components.table')
	@slot('table_header')
	<th style="width: 15%">Date</th>
	<th style="width: 15%">Season</th>
	<th style="width: 27%">Team 1</th>
	<th style="width: 27%">Team 2</th>
	<th></th>
	@endslot
	
	@slot('table_body')
	@foreach($list as $entry)
		<tr>
			<td>{{ $entry->present()->formattedDate }}</td>
			<td class="text-center">{{ $entry->present()->seasonName }}</td>
			<td class="text-center">{{ $entry->present()->team1Name }}</td>
			<td class="text-center">{{ $entry->present()->team2Name }}</td>
			<td>
				<a href="{{ route('admin.match.edit', ['match' => $entry->id]) }}" class="control-options primary"><i
						class="fa fa-pencil"></i></a>
				<a href="{{ route('admin.match.show', ['match' => $entry->id]) }}" class="control-options danger"><i
						class="fa fa-times"></i></a>
			</td>
		</tr>
	@endforeach
	@endslot
	@endcomponent
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('.indexDataTables').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"columns": [
					{"width": "15%", "targets": 0},
					{"width": "15%", "targets": 1},
					{"width": "25%", "targets": 2},
					{"width": "25%", "targets": 3},
					{"searchable": false, "targets": 4}
				]
			});
		})
	</script>
@endsection