@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matchs</a></span></li>
	<li><span>{{ $pageTitle }}</span></li>
@endsection

@section('admin_content')
	@include('components.admin.delete', ['object' => $match, 'name' => $match->present()->full_name, 'actionRoute' => route('admin.match.update', ['match' => $match->id]), 'indexRoute' => route('admin.match.index')])
@endsection