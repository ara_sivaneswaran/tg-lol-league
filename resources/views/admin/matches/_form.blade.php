@inject('service', 'App\Services\FormService')

{!! BootForm::date('Date', 'date')->required() !!}
<div class="form-group">
	<label class="col-sm-4 col-lg-2 control-label" for="time">Time</label>
	<div class="col-sm-8 col-lg-10">
		<input type="time" name="time" id="time" class="form-control" required="required" @if(isset($match))value="{{ $match->time }}"@endif>
	</div>
</div>
{!! BootForm::select('Season', 'season_id')->options($service->seasonsArray())->required() !!}
{!! BootForm::select('Team 1', 'team1_id')->options($service->teamsArray())->required() !!}
{!! BootForm::select('Team 2', 'team2_id')->options($service->teamsArray())->required() !!}
{!! BootForm::select('Type', 'type')->options($service->matchTypesArray())->required() !!}
<div class="form-group">
	<label class="col-sm-4 col-lg-2 control-label" for="best_of">Best of</label>
	<div class="col-sm-8 col-lg-10">
		<input type="number" name="best_of" id="best_of" class="form-control" required="required" @if(isset($match))value="{{ $match->best_of }}"@endif>
	</div>
</div>

{!! BootForm::checkbox('Featured', 'featured') !!}