@extends('layouts.admin')

@section('admin_breadcrumb')
	<li><span><a href="{{ route('admin.dashboard') }}">Admin</a></span></li>
	<li><span><a href="{{ route('admin.match.index') }}">Manage matchs</a></span></li>
	<li><span>Create match</span></li>
@endsection

@section('admin_content')
	@include('components.admin.forms.header', ['title' =>  '<strong>Create</strong> Match'])
	
	{!! BootForm::openHorizontal(['sm' => [4, 8],'lg' => [2, 10]])->post()->action(route('admin.match.store')) !!}
	
	@include('admin.matches._form')
	
	@include('components.admin.forms.footer', ['route' =>  route('admin.match.index'), 'submitText' => 'Create'])
	{!! BootForm::close() !!}
@endsection