@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span>Players</span></li>
@endsection

@section('main_content')
	<section class="darkgrey_section">
		<div class="container">
			<div class="row classes">
				<div class="col-md-4">
					<img src="{{ route('img', ['path' => $player->present()->photoUrl . '?w=200']) }}"
					     alt="{{ $player->summoner_name }}"/>
				</div>
				<div class="col-md-6">
					<h3>{{ $player->present()->full_name }}</h3>
					
					<div class="row">
						<div class="col-md-3">
							<div class="spacer-10"></div>
							
							<img src="{{ route('img', ['path' => $player->present()->teamLogo . '?w=100']) }}"
							     alt="{{ $player->present()->teamName }}"/>
						</div>
						
						<div class="col-md-9">
							<div class="spacer-20"></div>

							<div class="col-md-7">
								<h4>{{ $player->present()->primaryRole }} /
									<small>{{ $player->present()->secondaryRole }}</small>
								</h4>
								<h5>{{  $player->present()->teamName }}</h5>
							</div>
							<div class="col-md-5">
								<a href="http://na.op.gg/summoner/userName={{ $player->summoner_name }}" class="btn btn-primary"
								   target="_blank">OP.gg profile</a>
							</div>
						</div>
					</div>
					<div class="row text-center">
						<div class="col-md-3 bordered-box col-md-offset-1">
							<h4>{{ $profile['stats']['kda'] }} KDA</h4>
						</div>
						
						<div class="col-md-3 bordered-box">
							<h4>{{ $profile['stats']['damage'] }} Dmg</h4>
						</div>

						<div class="col-md-3 bordered-box">
							<h4>{{ $profile['stats']['kp'] }} KP%</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="color_section">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<h2 class="block-header animated fadeInUp"><strong>Most played champions</strong></h2>
				</div>
			</div>
			<div class="row">
				<div class="table-responsive">
					<table class="table table-striped text-center">
						<thead>
						<tr>
							<th colspan="2">Champion</th>
							<th>Games played</th>
							<th>Record</th>
							<th>KDA Ratio</th>
							<th>Stats</th>
							<th>Damage</th>
							<th>KP %</th>
						</tr>
						</thead>
						<tbody>
						@foreach($profile['champStats'] as $champion)
							<tr>
								<td><img
										src="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/{{ $champion['photo'] }}"
										alt=""{{ $champion['name'] }} style="width: 75px">
								</td>
								<td>{{ $champion['name'] }}</td>
								<td>{{ $champion['gp'] }}</td>
								<td>{{ $champion['wins'] }}-{{ $champion['loss'] }}</td>
								<td>{{ $champion['kda'] }}</td>
								<td>{{ $champion['kills'] }}/{{ $champion['deaths'] }}/{{ $champion['assists'] }}</td>
								<td>{{ $champion['damage'] }}</td>
								<td>{{ $champion['kp'] }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	
	<section class="darkgrey_section">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-12">
					<h2 class="block-header animated fadeInUp"><strong>Match history</strong></h2>
				</div>
			</div>
			<div class="row">
				<div class="table-responsive">
					<table id="match-history" class="table table-striped text-center">
						<thead>
						<tr>
							<th>Champion</th>
							<th>Opponent</th>
							<th>Date</th>
							<th>KDA</th>
							<th>KDA Ratio</th>
							<th>Damage</th>
							<th>KP %</th>
						</tr>
						</thead>
						<tbody>
						@foreach($games as $game)
							<tr class="linked" data-link="{{ route('match', ['matchId' => $game->game->match_id]) }}">
								<td>{!! $game->present()->champResultIcon !!}
									<img
										src="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/{{ $game->championM->image }}"
										alt="" {{ $game->championM->name }} style="width: 75px">
								</td>
								<td>{{ $game->present()->opponentName }}</td>
								<td>{{ $game->game->match->present()->humanDate }}</td>
								<td>{{ $game->present()->formatStats }}</td>
								<td>{{ $game->present()->kda }}</td>
								<td>{{ $game->present()->damage }}</td>
								<td>{{ $game->present()->kpPct }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
			</div>
		</div>
	</section>
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function() {
			$('table#match-history tbody tr').click(function() {
				location.href = $(this).data("link");
			});
		});
	</script>
@endsection