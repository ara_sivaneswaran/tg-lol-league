@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span>Schedule</span></li>
@endsection

@section('main_content')
	<section id="schedule" class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp"><strong>{{ $pageTitle }}</strong></h2>
				</div>
			</div>
			<div class="row classes">
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th style="width: 5%">ID</th>
								<th style="width: 10%">Date</th>
								<th style="width: 10%">Time</th>
								<th style="width: 30%">Team 1</th>
								<th style="width: 30%">Team 2</th>
								<th style="width: 15%"></th>
							</tr>
						</thead>
						<tbody>
							@foreach($matches as $match)
								<tr>
									<td>{{ $match->id }}</td>
									<td>{{ $match->present()->formattedDate }}</td>
									<td>{{ $match->present()->formatTime }}</td>
									<td>{!! $match->present()->team1Url !!}</td>
									<td>{!! $match->present()->team2Url !!}</td>
									<td>{!! $match->present()->url !!}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@endsection