@extends('layouts.site')

@section('content')
	<section id="land" class="parallax">
		
		<div id="slide1" class="land parallax shown">
			<div class="slide_description">
				<h3>Welcome to <span class="highlight">TG's</span> LoL Division</h3>
				<h2>The Tournament</h2>
			</div>
		</div>
		
		<div id="slide2" class="land parallax">
			<div class="slide_description">
				<h3>Where teams battle out to see who's the <span class="highlight">best</span></h3>
			</div>
		</div>
		
		<div id="slide3" class="land parallax">
			<div class="slide_description">

			</div>
		</div>
	</section>
	
	
	<section id="slide_tabs" class="dark_section bg_image">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<ul class="nav nav-tabs nav-justified main-tab">
						<li class="active">
							<a data-toggle="tab" class="slide1" href="#1">Featured match</a>
						</li>
						<li class="">
							<a data-toggle="tab" class="slide2" href="#2">Top teams</a>
						</li>
						<li class="">
							<a data-toggle="tab" class="slide3" href="#3">Top players</a>
						</li>
					</ul>
					<div class="tab-content">
						<div id="1" class="tab-pane fade active in">
							<div class="row">
								@if($featured_match)
									<div class="col-md-4 col-sm-6">
										<div class="media">
											<a class="pull-left" href="#">
												<img class="media-object no-bg"
												     src="{{ route('img', ['path' => $featured_match->team1->present()->imageUrl . '?w=125']) }}"
												     alt="">
											</a>
											<div class="media-body">
												<h4 class="media-heading">{{ $featured_match->team1->name }}</h4>
												<p>{{ $featured_match->team1->description }}</p>
											</div>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-6">
										<div class="media">
											<a class="pull-left" href="#">
												<img class="media-object no-bg"
												     src="{{ route('img', ['path' => $featured_match->team2->present()->imageUrl . '?w=125']) }}"
												     alt="">
											</a>
											<div class="media-body">
												<h4 class="media-heading">{{ $featured_match->team2->name }}</h4>
												<p>{{ $featured_match->team2->description }}</p>
											</div>
										</div>
									</div>
									
									<div class="col-md-4 col-sm-12">
										<p>
											<i class="fa fa-calendar"></i>
											<strong>{{ $featured_match->present()->formattedDate }}</strong>
										</p>
										<p>
											<i class="fa fa-clock-o"></i> <strong>{{ $featured_match->time }}</strong>
										</p>
									</div>
								@else
									<div class="col-md-12 col-sm-12">
										<h3>No upcoming matches</h3>
									</div>
								@endif
							</div>
						</div> <!-- #1 -->
						
						
						<div id="2" class="tab-pane fade">
							
							<div class="row">
								@foreach($top_teams as $team)
									<div class="col-sm-4 col-sm-6">
										<div class="media">
											<a class="pull-left" href="#">
												<img class="media-object no-bg"
												     src="{{ route('img', ['path' => $team['imageUrl'] . '?w=125']) }}"
												     alt="">
											</a>
											<div class="media-body">
												<h4 class="media-heading">{{ $team['team'] }}</h4>
												
												<dl class="dl-horizontal">
													<dt>Games played:</dt>
													<dd>{{ $team['gp'] }}</dd>
													<dt>Matches record:</dt>
													<dd>{{ $team['wins'] }}-{{ $team['loss'] }}</dd>
													<dt>Games record:</dt>
													<dd>{{ $team['games_won'] }}-{{ $team['games_lost'] }}</dd>
												</dl>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						
						
						</div> <!-- #2 -->
						
						
						<div id="3" class="tab-pane fade">
							<div class="row">
								@foreach($top_players as $player)
									<div class="col-sm-6 col-md-4">
										<div class="media">
											<a class="pull-left" href="#">
												<img class="media-object no-bg"
												     src="{{ route('img', ['path' => $player->player->present()->photoUrl . '?w=125']) }}"
												     alt="{{ $player->player->present()->full_name }}">
											</a>
											<div class="media-body">
												<h4 class="media-heading">{{ $player->player->summoner_name }}</h4>
												
												<dl class="dl-horizontal small-dt">
													<dt>Stats:</dt>
													<dd>{{ $player->present()->formatStats }}</dd>
													<dt>KDA:</dt>
													<dd>{{ $player->present()->kda }}</dd>
													<dt>Damage:</dt>
													<dd>{{ $player->damage }}</dd>
												</dl>
											</div>
										</div>
									</div>
								@endforeach
							</div>
						
						</div> <!-- #3 -->
					</div> <!-- .tab-content -->
				</div> <!-- .col-sm-12 -->
			</div> <!-- .row -->
		</div> <!-- .container -->
	</section><!-- #slide_tabs -->
	
	
	<section id="info" class="dark_section bg_image">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h1>League Tournament</h1>
					<h3>for <strong><span class="highlight">TG LoL squads</span></strong></h3>
					<p>
						This is a league where TG squads from the League of Legends division can play each other.
						Each season, the squads will be able to play against each other to test their skills, get better
						and have fun.
					</p>
				</div>
			</div>
		</div>
	</section>
	
	
	<section id="events" class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header"><strong>Upcoming</strong> Matches</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="owl-carousel owl-items-4 classes">
						@foreach($next_matches as $match)
							<div class="owl-carousel-item">
								<a href="class-single.html">
									<img src="example/horizontal_slider1.jpg" alt="Owl Image">
								</a>
								
								<h3>
									<a href="#">{{ $match->team1->name }} vs {{ $match->team2->name }}</a>
								</h3>
								<div class="classes-description">
									<p><i class="fa fa-calendar"></i> {{ $match->present()->formattedDate }}</p>
									@if($match->firstGame)
										<p><i class="fa fa-clock-o"></i> {{ $match->firstGame->time }}</p>
									@endif
									<p><i class="fa fa-laptop"></i> <a href="#">Match page</a></p>
								</div>
							</div>
						@endforeach
					</div> <!-- eof owl-carousel -->
				
				
				</div>
			</div>
		</div>
	</section>
@endsection