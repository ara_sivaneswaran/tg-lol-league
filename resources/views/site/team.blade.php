@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span><a href="{{ route('teams') }}">Teams</a></span></li>
	<li><span>{{ $team->name }}</span></li>
@endsection

@section('main_content')
	<section class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp">
						<img src="{{ route('img', ['path' => $team->present()->imageUrl . '?w=75']) }}"
						alt="{{ $team->name }}">
						<strong>{{ $pageTitle }}</strong>
					</h2>
				</div>
			</div>
			<div class="row classes">
				<div class="col-md-8">
					<div class="table-responsive">
						<table id="roster" class="table table-striped text-center">
							<tbody>
								@foreach($roster as $player)
				               		<tr class="linked"  data-link="{{ route('player', ['playerId' => $player->id]) }}">
				               			<td style="width: 15%;">
					                    	<img src="{{ route('img', ['path' => $player->present()->photoUrl . '?w=75&h=100']) }}"
											alt="{{ $player->summoner_name }}" />
					                    </td>
					                    <td style="width: 40%; vertical-align: middle;">
					                        <p>Primary Role: {{ $player->present()->primaryRole }}</p>
					                        <p>Secondary Role: {{ $player->present()->secondaryRole }}</p>
					                    </td>
					                    <td style="vertical-align: middle;">{{ $player->present()->full_name }}</td>
				               		</tr>
			                	@endforeach
							</tbody>
						</table>
					</div>
                    </ul>
				</div>
				<div class="col-md-4">
					<dl class="dl-horizontal">
						<dt>Record:</dt>
						<dd>@if($stats) { $stats->present()->record }} @else 0-0-0 @endif</dd>
						<dt>Win %:</dt>
						<dd>@if($stats) {{ $stats->present()->winPct }} @else 0 @endif % </dd>
						<dt>Games:</dt>
						<dd>@if($stats) {{ $stats->games_won }} - {{ $stats->games_lost }} @else 0 - 0 @endif</dd>
					</dl>
					@if($team->description) <blockquote>{{ $team->description }}</blockquote> @endif
				</div>
			</div>
		</div>
	</section>
	<section class="color_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp"><strong>Schedule</strong></h2>
				</div>
			</div>
			<div class="row">
				<div class="table-responsive">
					<table class="table table-striped text-center">
						<thead>
							<tr>
								<th>Date</th>
								<th>Time</th>
								<th>Team 1</th>
								<th>Team 2</th>
								<th>Result</th>
							</tr>
						</thead>
						<tbody>
							@foreach($matches as $match)
								<tr>
									<td>{{ $match->present()->formattedDate }}</td>
									<td>{{ $match->present()->formatTime }}</td>
									<td>{!! $match->present()->team1Url !!}</td>
									<td>{!! $match->present()->team2Url !!}</td>
									<td>
										@if($match->finished && $match->present()->winnerId == $team->id)
											<span class="text-success">
										@elseif($match->finished)
											<span class="text-danger">
										@else
											<span>
										@endif
												{!! $match->present()->resultUrl !!}
											</span>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('table#roster tbody tr').click(function () {
				location.href = $(this).data("link");
			});
		});
	</script>
@endsection