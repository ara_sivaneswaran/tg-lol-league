@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span><a href="{{ route('schedule') }}">Schedule</a></span></li>
	<li><span>Match # {{ $match->id }} ({{ $pageTitle }})</span></li>
@endsection

@section('main_content')
	<section class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 text-left">
					<h4>{{ $match->present()->readableDate }} - {{ $match->present()->formatTime }}</h4>
				</div>
				<div class="col-md-6 col-sm-12 text-right">
					<h4>{{ $match->present()->formatType }} - {{ $match->present()->bestOf }}</h4>
				</div>
				<div class="clearfix"></div>
				<hr/>
			</div>
			<div class="row classes">
				<div class="col-md-6 col-sm-12 text-right">
					<div class="col-md-8 col-sm-12 text-">
						<h4>{{ $match->team1->name }}<h4>
						<p>{{ $match->team1_score }}</p>
					</div>
					<div class="col-md-4 col-sm-12">
						<a href="{{ route('team', ['teamId' => $match->team1_id]) }}">
							<img src="{{ route('img', ['path' => $match->team1->present()->imageUrl . '?w=175']) }}"
							alt="{{ $match->team1->name }}" class="no-bg"/>
						</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 text-left">
					<div class="col-md-4 col-sm-12">
						<a href="{{ route('team', ['teamId' => $match->team2_id]) }}">
							<img src="{{ route('img', ['path' => $match->team2->present()->imageUrl . '?w=175']) }}"
							     alt="{{ $match->team1->name }}" class="no-bg"/>
						</a>
					</div>
					<div class="col-md-8 col-sm-12">
						<h4>{{ $match->team2->name }}<h4>
						<p>{{ $match->team2_score }}</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	@foreach($match->games as $game)
		<section @if($game->match_number % 2 != 0) class="color_section" @else class="darkgrey_section" @endif>
			<div class="container">
				<div class="row">
	                <div class="col-sm-12 text-center">
	                    <h2 class="block-header">Game #<strong>{{ $game->match_number }}</strong></h2>
	                </div>
	            </div>

	            <div class="row">
	            	<div class="col-md-3">
	            		Winner: @if($game->winner == $game->blue_team_id) {{ $game->blue_team->name }} @else {{ $game->red_team->name }} @endif
        			</div>
	            	<div class="col-md-3">
	            		<div class="col-md-5 text-information">{{ $game->boxScores()->where('team_id', $game->blue_team_id)->first()->present()->formatStats }}</div>
	            		<div class="col-md-2">Vs.</div> 
	            		<div class="col-md-5 text-danger">{{ $game->boxScores()->where('team_id', $game->red_team_id)->first()->present()->formatStats }}</div>
        			</div>
        			<div class="col-md-3">
        				@if($game->match_history_link) <a href="{{ $game->match_history_link }}" target="_blank">More stats</a> @endif
        			</div>
        			<div class="col-md-3">
        				@if($game->video_link) <a href="{{ $game->video_link }}" target="_blank">Video</a> @endif
        			</div>
	            </div>

				<div class="row">
					<div class="col-md-6">
						<h3 class="text-information">Blue side: {{ $game->blue_team->name }}</h3>

						<p class="no-margin"><strong>Bans</strong></p>
						<div class="greyscale">
							@foreach($bansAndPicks[$game->id]['blue']['bans'] as $ban)
								{!! $ban !!}
							@endforeach
						</div>

						<div class="spacer-20"></div>

						<p class="no-margin"><strong>Picks</strong></p>
							@foreach($bansAndPicks[$game->id]['blue']['picks'] as $pick)
								{!! $pick !!}
							@endforeach

						<div class="spacer-20"></div>

						<div class="table-responsive">
							<table class="table table-striped text-center roster">
								<thead>
									<tr>
										<td colspan="2">Player</td>
										<td>Role</td>
										<td>KDA</td>
										<td>Stats</td>
										<td>KP %</td>
										<td>Dmg</td>
									</tr>
								</thead>
								<tbody>
									@foreach($playerStats[$game->id]['blue'] as $role => $player)
										<tr class="linked" data-link="{{ route('player', ['playerId' => $player['id']])  }}">
											<td>{!! $player['champion'] !!}</td>
											<td>{{ $player['name'] }}</td>
											<td>{{ $player['role'] }}</td>
											<td>{{ $player['kda'] }}</td>
											<td>{{ $player['stats'] }}</td>
											<td>{{ $player['kp'] }}</td>
											<td>{{ $player['damage'] }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-md-6">
						<h3 class="text-danger">Red side: {{ $game->red_team->name }}</h3>

						<p class="no-margin"><strong>Bans</strong></p>
						<div class="greyscale">
							@foreach($bansAndPicks[$game->id]['red']['bans'] as $ban)
								{!! $ban !!}
							@endforeach
						</div>

						<div class="spacer-20"></div>

						<p class="no-margin"><strong>Picks</strong></p>
							@foreach($bansAndPicks[$game->id]['red']['picks'] as $pick)
								{!! $pick !!}
							@endforeach

						<div class="spacer-20"></div>

						<div class="table-responsive">
							<table class="table table-striped text-center roster">
								<thead>
									<tr>
										<td colspan="2">Player</td>
										<td>Role</td>
										<td>KDA</td>
										<td>Stats</td>
										<td>KP %</td>
										<td>Dmg</td>
									</tr>
								</thead>
								<tbody>
									@foreach($playerStats[$game->id]['red'] as $role => $player)
										<tr class="linked" data-link="{{ route('player', ['playerId' => $player['id']])  }}">
											<td>{!! $player['champion'] !!}</td>
											<td>{{ $player['name'] }}</td>
											<td>{{ $player['role'] }}</td>
											<td>{{ $player['kda'] }}</td>
											<td>{{ $player['stats'] }}</td>
											<td>{{ $player['kp'] }}</td>
											<td>{{ $player['damage'] }}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	@endforeach
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('table.roster tbody tr').click(function () {
				location.href = $(this).data("link");
			});
		});
	</script>
@endsection