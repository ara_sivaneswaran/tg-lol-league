@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span><a href="{{ route('schedule') }}">Schedule</a></span></li>
	<li><span>Match # {{ $match->id }} ({{ $pageTitle }})</span></li>
@endsection

@section('main_content')
	<section class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-12 text-left">
					<h4>{{ $match->present()->readableDate }} - {{ $match->present()->formatTime }}</h4>
				</div>
				<div class="col-md-6 col-sm-12 text-right">
					<h4>{{ $match->present()->formatType }} - {{ $match->present()->bestOf }}</h4>
				</div>
				<div class="clearfix"></div>
				<hr/>
			</div>
			<div class="row classes">
				<div class="col-md-6 col-sm-12 text-right">
					<div class="col-md-8 col-sm-12 text-">
						<h4>{{ $match->team1->name }}<h4>
						<p>
							@if($team1Stats)
								{{ $team1Stats->present()->record }}
						    @else
								0W - 0L
						    @endif
						</p>
					</div>
					<div class="col-md-4 col-sm-12">
						<a href="{{ route('team', ['teamId' => $match->team1_id]) }}">
							<img src="{{ route('img', ['path' => $match->team1->present()->imageUrl . '?w=175']) }}"
							alt="{{ $match->team1->name }}" class="no-bg"/>
						</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-12 text-left">
					<div class="col-md-4 col-sm-12">
						<a href="{{ route('team', ['teamId' => $match->team2_id]) }}">
							<img src="{{ route('img', ['path' => $match->team2->present()->imageUrl . '?w=175']) }}"
							     alt="{{ $match->team1->name }}" class="no-bg"/>
						</a>
					</div>
					<div class="col-md-8 col-sm-12">
						<h4>{{ $match->team2->name }}<h4>
						<p>
							@if($team2Stats)
								{{ $team2Stats->present()->record }}
							@else
								0W - 0L
							@endif
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="darkgrey_section">
		<div class="container">
			<div class="row classes">
				<div class="col-md-6 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped text-center roster">
							<tbody>
								@foreach($team1Roster as $player)
				               		<tr class="linked" data-link="{{ route('player', ['playerId' => $player->id])  }}">
					                    <td style="vertical-align: middle;">{{ $player->summoner_name }}</td>
					                    <td style="width: 40%; vvertical-align: middle;">
					                        <p>Primary Role: {{ $player->present()->primaryRole }}</p>
					                        <p>Secondary Role: {{ $player->present()->secondaryRole }}</p>
					                    </td>
					                    <td style="width: 25%;">
					                    	<img src="{{ route('img', ['path' => $player->present()->photoUrl . '?w=75&h=100']) }}"
											alt="{{ $player->summoner_name }}" />
					                    </td>
				               		</tr>
			                @endforeach
							</tbody>
						</table>
					</div>
	            </div>
	            <div class="col-md-6 col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped text-center roster">
							<tbody>
								@foreach($team2Roster as $player)
				               		<tr class="linked" data-link="{{ route('player', ['playerId' => $player->id])  }}">
				               			<td style="width: 25%;">
					                    	<img src="{{ route('img', ['path' => $player->present()->photoUrl . '?w=75&h=100']) }}"
											alt="{{ $player->summoner_name }}" />
					                    </td>
					                    <td style="width: 40%; vertical-align: middle;">
					                        <p>Primary Role: {{ $player->present()->primaryRole }}</p>
					                        <p>Secondary Role: {{ $player->present()->secondaryRole }}</p>
					                    </td>
					                    <td style="vertical-align: middle;">{{ $player->summoner_name }}</td>
				               		</tr>
			                	@endforeach
							</tbody>
						</table>
					</div>
	            </div>
			</div>
		</div>
	</section>
	<section class="color_section">
		<div class="container">
			<div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="block-header"><strong>Players</strong> to watch</h2>
                </div>
            </div>
			<div class="row">
				<div class="col-md-6 col-sm-12">
					@foreach($team1TopPlayers as $player)
						<div class="slide media darkgrey_section">
	                        <a href="#" class="pull-right">
	                            <img src="{{ route('img', ['path' => $player->player->present()->photoUrl . '?w=150']) }}" alt="{{ $player->player->summoner_name }}" class="media-object">
	                        </a>

	                        <div class="media-body text-right">
	                            <h3>{{ $player->player->summoner_name}}</h3>
	                            <p><a href="#">{{ $player->player->present()->primaryRole() }}</a></p>
	                            <dl class="dl-horizontal">
									<dt>Stats:</dt>
									<dd>{{ $player->present()->formatStats }}</dd>
									<dt>KDA:</dt>
									<dd>{{ $player->present()->kda }}</dd>
									<dt>Damage:</dt>
									<dd>{{ $player->damage }}</dd>
								</dl>
	                        </div>
	                    </div>
                    @endforeach
					
					@if($team1TopPlayers->count() == 0)
						<h3>No player stats available for {{ $match->team1->name }}.</h3>
					@endif
                </div>
                <div class="col-md-6 col-sm-12">
					@foreach($team2TopPlayers as $player)
						<div class="slide media darkgrey_section">
	                        <a href="#" class="pull-left">
	                            <img src="{{ route('img', ['path' => $player->player->present()->photoUrl . '?w=150']) }}" alt="{{ $player->player->summoner_name }}" class="media-object">
	                        </a>

	                        <div class="media-body text-left">
	                            <h3>{{ $player->player->summoner_name }}</h3>
	                            <p><a href="#">{{ $player->player->present()->primaryRole() }}</a></p>
	                            <dl class="dl-horizontal">
									<dt>Stats:</dt>
									<dd>{{ $player->present()->formatStats }}</dd>
									<dt>KDA:</dt>
									<dd>{{ $player->present()->kda }}</dd>
									<dt>Damage:</dt>
									<dd>{{ $player->damage }}</dd>
								</dl>
	                        </div>
	                    </div>
                    @endforeach
	                
					@if($team2TopPlayers->count() == 0)
						<h3>No player stats available for {{ $match->team2->name }}.</h3>
					@endif
                </div>
            </div>
        </div>
	</section>
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('table.roster tbody tr').click(function () {
				location.href = $(this).data("link");
			});
		});
	</script>
@endsection