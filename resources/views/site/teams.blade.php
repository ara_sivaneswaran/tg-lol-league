@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span>Schedule</span></li>
@endsection

@section('main_content')
	<section id="schedule" class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp"><strong>{{ $pageTitle }}</strong></h2>
				</div>
			</div>
			<div class="row classes">
				<div id="portfolio_wrapper" class="row">
	                <div class="text-center filters col-sm-12">
	                    <ul id="filtrable">
	                        <li><a class="selected" data-filter="*" href="#">All</a></li>
	                        @foreach($groups as $group)
	                        	<li><a data-filter=".group-{{ $group->id }}" href="#" class="">{{ $group->name }}</a></li>
                        	@endforeach
	                    </ul>
	                    <div class="clearfix"></div>
	                </div>


	                <ul class="classes row filtrable clearfix isotope" id="portfolioContainer">
	                	@foreach($teams as $team)
		                    <li class="item col-sm-6 col-md-4 isotope-item group-{{ $team->groupsBySeason($seasonId)->id }}"> 
		                        <h3>
		                        	<img src="{{ route('img', ['path' => $team->present()->imageUrl . '?w=50']) }}"
									alt="{{ $team->name }}">
									
		                        	{!! $team->present()->url !!}
	                        	</h3>
		                        <div class="classes-description">
		                            <p>{{ $team->groupsBySeason($seasonId)->name }} group</p>
		                            <p>{{ $team->players()->count() }} players</p>
		                        </div>
		                    </li>
                    	@endforeach
	                </ul>
				</div>
			</div>
		</div>
	</section>
@endsection