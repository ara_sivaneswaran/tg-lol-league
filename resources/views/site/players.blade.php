@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span>Players</span></li>
@endsection

@section('main_content')
	<section id="schedule" class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp"><strong>{{ $pageTitle }}</strong></h2>
				</div>
			</div>
			<div class="row classes">
				<div class="table-responsive">
					<table class="table table-hover table-striped text-center" id="playersTable">
						<thead>
						<tr>
							<th>Player</th>
							<th style="width: 10%">GP</th>
							<th style="width: 10%">KDA</th>
							<th style="width: 10%">KP %</th>
							<th style="width: 10%">Kills</th>
							<th style="width: 10%">Deaths</th>
							<th style="width: 10%">Assists</th>
							<th style="width: 15%">Damage</th>
						</tr>
						</thead>
						<tbody>
						@foreach($playerStats as $player)
							<tr>
								<td>{!! $player->player->present()->url !!}</td>
								<td>{{ $player->gp }}</td>
								<td>{{ $player->present()->KDA }}</td>
								<td>{{ $player->present()->kpPct }}</td>
								<td>{{ $player->kills }}</td>
								<td>{{ $player->deaths }}</td>
								<td>{{ $player->assists }}</td>
								<td>{{ $player->present()->avgDmg }}</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
@endsection

@section('inline-scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$('#playersTable').DataTable({
				"dom": '<"top"f>rt<"bottom"pli><"clear">',
				"order": [[2, 'desc'], [3, 'desc'],  [1, 'asc']]
			});
		});
	</script>
@endsection