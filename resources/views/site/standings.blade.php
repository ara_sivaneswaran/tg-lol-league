@extends('layouts.site_normal')

@section('breadcrumbs')
	<li><span>Standings</span></li>
@endsection

@section('main_content')
	<section id="schedule" class="darkgrey_section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="block-header animated fadeInUp"><strong>{{ $pageTitle }}</strong></h2>
				</div>
			</div>
			<div class="row classes">
				@foreach($groups as $group)
					<div class="col-md-6 col-sm-12">
						<div class="table-responsive">
							<table class="table table-hover table-striped">
								<thead>
								<tr>
									<th colspan="7">{{ $group->name }}</th>
								</tr>
								<tr>
									<th style="width: 5%"></th>
									<th style="width: 10%">Team</th>
									<th style="width: 10%">GP</th>
									<th style="width: 10%">Wins</th>
									<th style="width: 10%">Loss</th>
									<th style="width: 10%">GW</th>
									<th style="width: 10%">GL</th>
								</tr>
								</thead>
								<tbody>
								@foreach($standings->where('group_id', $group->id) as $team)
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>
											<a href="{{ route('team', ['teamId' => $team['team_id']]) }}">{{ $team['team'] }}</a>
										</td>
										<td>{{ $team['gp'] }}</td>
										<td>{{ $team['wins'] }}</td>
										<td>{{ $team['loss'] }}</td>
										<td>{{ $team['games_won'] }}</td>
										<td>{{ $team['games_lost'] }}</td>
									</tr>
								@endforeach
								</tbody>
							</table>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>
@endsection