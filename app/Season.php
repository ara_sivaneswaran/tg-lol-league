<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Laracasts\Presenter\PresentableTrait;

class Season extends Model
{
	use SoftDeletes;
	use PresentableTrait;
	
	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\SeasonPresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'seasons';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at', 'deleted_at'];
	
	/**
	 * Model's relationships
	 */
	public function groups ()
	{
		return $this->belongsToMany(Group::class, 'groups_seasons', 'season_id', 'group_id');
	}
	
	public function matches ()
	{
		return $this->hasMany(Match::class);
	}
	
	public function teams ()
	{
		return $this->belongsToMany(Team::class, 'teams_seasons', 'season_id', 'team_id');
	}
}