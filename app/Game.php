<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
	use SoftDeletes;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'games';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'match_id',
		'blue_team_id',
		'red_team_id',
		'match_number',
		'match_history_link',
		'video_link',
		'duration',
		'finished',
		'winner',
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = ['finished' => 'boolean'];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	/**
	 * Model's relationships
	 */
	public function blue_team ()
	{
		return $this->hasOne(Team::class, 'id', 'blue_team_id');
	}
	
	public function boxScores ()
	{
		return $this->hasMany(GameBoxScore::class);
	}
	
	public function match ()
	{
		return $this->belongsTo(Match::class);
	}
	
	public function playerStats ()
	{
		return $this->hasMany(GamePlayerStats::class);
	}
	
	public function red_team ()
	{
		return $this->hasOne(Team::class, 'id', 'red_team_id');
	}
	
	public function stats ()
	{
		return $this->hasOne(GameStats::class);
	}
}