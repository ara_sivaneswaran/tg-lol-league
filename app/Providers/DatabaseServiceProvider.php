<?php

namespace App\Providers;

use App\Game;
use App\GameBoxScore;
use App\GamePlayerStats;
use App\GameStats;
use App\Group;
use App\Match;
use App\Player;
use App\PlayerStats;
use App\Repositories\Decorators\Cache\CacheGameBoxScoreRepository;
use App\Repositories\Decorators\Cache\CacheGamePlayerStatsRepository;
use App\Repositories\Decorators\Cache\CacheGameRepository;
use App\Repositories\Decorators\Cache\CacheGameStatsRepository;
use App\Repositories\Decorators\Cache\CacheGroupRepository;
use App\Repositories\Decorators\Cache\CacheMatchRepository;
use App\Repositories\Decorators\Cache\CachePlayerRepository;
use App\Repositories\Decorators\Cache\CachePlayerStatsRepository;
use App\Repositories\Decorators\Cache\CacheSeasonRepository;
use App\Repositories\Decorators\Cache\CacheStandingsRepository;
use App\Repositories\Decorators\Cache\CacheTeamRepository;
use App\Repositories\Decorators\Cache\CacheTeamStatsRepository;
use App\Repositories\Decorators\Cache\CacheUserRepository;
use App\Repositories\EloquentGameBoxScoreRepository;
use App\Repositories\EloquentGamePlayerStatsRepository;
use App\Repositories\EloquentGameRepository;
use App\Repositories\EloquentGameStatsRepository;
use App\Repositories\EloquentGroupRepository;
use App\Repositories\EloquentMatchRepository;
use App\Repositories\EloquentPlayerRepository;
use App\Repositories\EloquentPlayerStatsRepository;
use App\Repositories\EloquentSeasonRepository;
use App\Repositories\EloquentTeamRepository;
use App\Repositories\EloquentTeamStatsRepository;
use App\Repositories\EloquentUserRepository;
use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\GameStatsRepositoryInterface;
use App\Repositories\Interfaces\GroupRepositoryInterface;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use App\Repositories\Interfaces\PlayerRepositoryInterface;
use App\Repositories\Interfaces\PlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\SeasonRepositoryInterface;
use App\Repositories\Interfaces\StandingsRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\ServiceStandingsRepository;
use App\Season;
use App\Services\StandingsService;
use App\Team;
use App\TeamStat;
use App\TeamStats;
use App\User;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot ()
	{
		//
	}
	
	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register ()
	{
		// Game Repository
		$this->app->singleton(GameRepositoryInterface::class, function () {
			return new CacheGameRepository(new EloquentGameRepository(new Game), $this->app['cache.store']);
		});
		
		// GameBoxScore Repository
		$this->app->singleton(GameBoxScoreRepositoryInterface::class, function () {
			return new CacheGameBoxScoreRepository(new EloquentGameBoxScoreRepository(new GameBoxScore), $this->app['cache.store']);
		});
		
		// GamePlayerStats Repository
		$this->app->singleton(GamePlayerStatsRepositoryInterface::class, function () {
			return new CacheGamePlayerStatsRepository(new EloquentGamePlayerStatsRepository(new GamePlayerStats), $this->app['cache.store']);
		});
		
		// GameStats Repository
		$this->app->singleton(GameStatsRepositoryInterface::class, function () {
			return new CacheGameStatsRepository(new EloquentGameStatsRepository(new GameStats), $this->app['cache.store']);
		});
		
		// Group Repository
		$this->app->singleton(GroupRepositoryInterface::class, function () {
			return new CacheGroupRepository(new EloquentGroupRepository(new Group), $this->app['cache.store']);
		});
		
		// Match Repository
		$this->app->singleton(MatchRepositoryInterface::class, function () {
			return new CacheMatchRepository(new EloquentMatchRepository(new Match), $this->app['cache.store']);
		});
		
		// Player Repository
		$this->app->singleton(PlayerRepositoryInterface::class, function () {
			return new CachePlayerRepository(new EloquentPlayerRepository(new Player), $this->app['cache.store']);
		});
		
		// Player Repository
		$this->app->singleton(PlayerStatsRepositoryInterface::class, function () {
			return new CachePlayerStatsRepository(new EloquentPlayerStatsRepository(new PlayerStats), $this->app['cache.store']);
		});
		
		// Season Repository
		$this->app->singleton(SeasonRepositoryInterface::class, function () {
			return new CacheSeasonRepository(new EloquentSeasonRepository(new Season), $this->app['cache.store']);
		});
		
		// Standings Repository
		$this->app->singleton(StandingsRepositoryInterface::class, function () {
			return new CacheStandingsRepository(new ServiceStandingsRepository(new StandingsService), $this->app['cache.store']);
		});
		
		// Team Repository
		$this->app->singleton(TeamRepositoryInterface::class, function () {
			return new CacheTeamRepository(new EloquentTeamRepository(new Team), $this->app['cache.store']);
		});
		
		// Team Stats Repository
		$this->app->singleton(TeamStatsRepositoryInterface::class, function () {
			return new CacheTeamStatsRepository(new EloquentTeamStatsRepository(new TeamStats), $this->app['cache.store']);
		});

		// User Stats Repository
		$this->app->singleton(UserRepositoryInterface::class, function () {
			return new CacheUserRepository(new EloquentUserRepository(new User), $this->app['cache.store']);
		});
	}
}
