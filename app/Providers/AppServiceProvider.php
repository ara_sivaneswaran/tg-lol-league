<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use League\Glide\ServerFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
	    if ($this->app->environment() == 'local') {
		    $this->app->register('\Laracademy\Generators\GeneratorsServiceProvider');
	    }
	
	    $this->app->singleton('League\Glide\Server', function ($app) {
		
		    $cloud = $app->make('Illuminate\Contracts\Filesystem\Cloud');
		    $local = $app->make('Illuminate\Contracts\Filesystem\Filesystem');
		
		    return ServerFactory::create([
			    'source'             => $cloud->getDriver(),
			    'cache'              => $local->getDriver(),
			    'source_path_prefix' => '',
			    'cache_path_prefix'  => 'img/.cache',
			    'base_url'           => 'img'
		    ]);
	    });
    }
}
