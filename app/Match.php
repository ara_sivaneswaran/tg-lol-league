<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

/**
 * Class Match
 * @package App
 */
class Match extends Model
{
	use SoftDeletes;
	use PresentableTrait;
	
	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\MatchPresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'matches';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'season_id',
		'date',
		'time',
		'type',
		'best_of',
		'team1_id',
		'team2_id',
		'featured',
		'team1_score',
		'team2_score',
		'finished',
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = ['featured' => 'boolean'];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['date', 'created_at', 'updated_at', 'deleted_at'];
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function games ()
	{
		return $this->hasMany(Game::class);
	}
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function season ()
	{
		return $this->belongsTo(Season::class);
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function team1 ()
	{
		return $this->hasOne(Team::class, 'id', 'team1_id');
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function team2 ()
	{
		return $this->hasOne(Team::class, 'id', 'team2_id');
	}
	
	/**
	 * Select games that are marked as featured
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeFeatured ($query)
	{
		return $query->where('featured', 1);
	}
	
	public function scopeFirstGame ($query)
	{
		return $query->games()->orderBy('match_number', 'asc')->first();
	}
	
	/**
	 * Select games that are ONLY in the future
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeFuture ($query)
	{
		return $query->where('date', '>', Carbon::now()->format('Y-m-d'));
	}
	
	/**
	 * Select games that are ONLY in the past
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePast ($query)
	{
		return $query->where('date', '<', Carbon::now()->format('Y-m-d'));
	}
	
	/**
	 * Select games that are in the past. Including same day
	 *
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopePastAndPresent ($query)
	{
		return $query->where('date', '<=', Carbon::now()->format('Y-m-d'));
	}
	
	/**
	 * Select games that in the future. Including same day
	 * @param $query
	 *
	 * @return mixed
	 */
	public function scopeFutureAndPresent ($query)
	{
		return $query->where('date', '>=', Carbon::now()->format('Y-m-d'));
	}
}