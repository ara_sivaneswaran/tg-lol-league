<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameStats extends Model
{
	use SoftDeletes;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'games_stats';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'game_id',
		'duration',
		'red_ban_1',
		'red_ban_2',
		'red_ban_3',
		'red_pick_1',
		'red_pick_2',
		'red_pick_3',
		'red_pick_4',
		'red_pick_5',
		'blue_ban_1',
		'blue_ban_2',
		'blue_ban_3',
		'blue_pick_1',
		'blue_pick_2',
		'blue_pick_3',
		'blue_pick_4',
		'blue_pick_5',
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
}