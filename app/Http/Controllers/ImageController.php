<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use League\Glide\Server;

class ImageController extends Controller
{
	/**
	 * @param Server  $server
	 * @param Request $request
	 */
	public function show (Server $server, Request $request)
	{
		$path       = $request->getPathInfo();
		$parameters = [
			'w' => $request->input('w'),
			'h' => $request->input('h'),
		];
		
		if ($request->has('filt')) {
			$parameters['filt'] = $request->input('filt');
		}
		
		if ($request->has('fit')) {
			$parameters['fit'] = $request->input('fit');
		}
		
		return $server->outputImage($path, $parameters);
	}
}