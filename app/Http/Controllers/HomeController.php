<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\GroupRepositoryInterface;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use App\Repositories\Interfaces\PlayerRepositoryInterface;
use App\Repositories\Interfaces\PlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\SeasonRepositoryInterface;
use App\Repositories\Interfaces\StandingsRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use App\Services\MatchService;
use App\Services\PlayerService;
use App\Services\StandingsService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
	/**
	 * @var GroupRepositoryInterface
	 */
	private $groupRepository;
	/**
	 * @var MatchRepositoryInterface
	 */
	private $matchRepository;
	/**
	 * @var StandingsRepositoryInterface
	 */
	private $standingsRepository;
	/**
	 * @var PlayerStatsRepositoryInterface
	 */
	private $playerStatsRepository;
	/**
	 * @var TeamRepositoryInterface
	 */
	private $teamRepository;
	/**
	 * @var TeamRepositoryInterface
	 */
	private $teamStatsRepository;
	/**
	 * @var PlayerRepositoryInterface
	 */
	private $playerRepository;
	/**
	 * @var GamePlayerStatsRepositoryInterface
	 */
	private $gamePlayerStatsRepository;
	/**
	 * @var SeasonRepositoryInterface
	 */
	private $seasonRepository;
	
	/**
	 * Create a new controller instance.
	 *
	 * @param GamePlayerStatsRepositoryInterface $gamePlayerStatsRepository
	 * @param GroupRepositoryInterface           $groupRepository
	 * @param MatchRepositoryInterface           $matchRepository
	 * @param PlayerRepositoryInterface          $playerRepository
	 * @param PlayerStatsRepositoryInterface     $playerStatsRepository
	 * @param StandingsRepositoryInterface       $standingsRepository
	 * @param TeamRepositoryInterface            $teamRepository
	 * @param TeamStatsRepositoryInterface       $teamStatsRepository
	 * @param SeasonRepositoryInterface          $seasonRepository
	 */
	public function __construct (GamePlayerStatsRepositoryInterface $gamePlayerStatsRepository, GroupRepositoryInterface $groupRepository, MatchRepositoryInterface $matchRepository, PlayerRepositoryInterface $playerRepository, PlayerStatsRepositoryInterface $playerStatsRepository, StandingsRepositoryInterface $standingsRepository, TeamRepositoryInterface $teamRepository, TeamStatsRepositoryInterface $teamStatsRepository, SeasonRepositoryInterface $seasonRepository)
	{
		$this->groupRepository           = $groupRepository;
		$this->matchRepository           = $matchRepository;
		$this->standingsRepository       = $standingsRepository;
		$this->playerRepository          = $playerRepository;
		$this->playerStatsRepository     = $playerStatsRepository;
		$this->teamRepository            = $teamRepository;
		$this->teamStatsRepository       = $teamStatsRepository;
		$this->gamePlayerStatsRepository = $gamePlayerStatsRepository;
		$this->seasonRepository          = $seasonRepository;
	}
	
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		if ($current_season) {
			$featured_match = $this->matchRepository->nextFeaturedMatches()->first();
			$top_teams      = $this->standingsRepository->bySeason($current_season)->take(3);
			$top_players    = $this->playerStatsRepository->findBySeason($current_season)->take(3);
			$next_matches   = $this->matchRepository->nextMatches()->take(8);
		}
		else {
			$featured_match = null;
			$top_teams = [];
			$top_players = [];
			$next_matches = [];
		}
		
		return view('site.home', [
			'featured_match' => $featured_match,
			'top_teams'      => $top_teams,
			'top_players'    => $top_players,
			'next_matches'   => $next_matches
		]);
	}
	
	public function schedule ()
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		if ($current_season)
			$matches = $this->matchRepository->bySeason($current_season->id, 25);
		else
			$matches = [];
		
		return view('site.schedule', ['pageTitle' => 'Schedule', 'matches' => $matches]);
	}
	
	public function match ($matchId)
	{
		$match           = $this->matchRepository->find($matchId);
		$team1Stats      = $this->teamStatsRepository->findByTeamSeason($match->team1_id, $match->season_id);
		$team2Stats      = $this->teamStatsRepository->findByTeamSeason($match->team2_id, $match->season_id);
		$team1Roster     = $this->teamRepository->roster($match->team1_id);
		$team2Roster     = $this->teamRepository->roster($match->team2_id);
		$team1TopPlayers = $this->playerStatsRepository->findBySeasonAndTeam($match->season_id, $match->team1_id)->take(3);
		$team2TopPlayers = $this->playerStatsRepository->findBySeasonAndTeam($match->season_id, $match->team2_id)->take(3);
		

		if ($match->finished){
			$service = new MatchService();

			$bansAndPicks = $service->formatBansAndPicks($match->games);
			$playerStats = $service->formatPlayerStats($match->games);

			return view('site.match.result', ['pageTitle' => 'Match result', 'match' => $match, 'bansAndPicks' => $bansAndPicks, 'playerStats' => $playerStats]);
		}
		else
			return view('site.match.pre-game', [
				'pageTitle'       => 'Pre-game',
				'match'           => $match,
				'team1Stats'      => $team1Stats,
				'team2Stats'      => $team2Stats,
				'team1Roster'     => $team1Roster,
				'team2Roster'     => $team2Roster,
				'team1TopPlayers' => $team1TopPlayers,
				'team2TopPlayers' => $team2TopPlayers
			]);
	}
	
	public function standings ()
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		if($current_season){
			$standings = $this->standingsRepository->bySeason($current_season->id);
			$groups    = $this->groupRepository->orderedAll();
		}
		else {
			$standings = [];
			$groups = [];
		}
		
		return view('site.standings', ['pageTitle' => 'Standings', 'standings' => $standings, 'groups' => $groups]);
	}
	
	public function teams ()
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		if($current_season)
			$teams  = $this->teamRepository->bySeason($current_season->id);
		else
			$teams = [];

		$groups = $this->groupRepository->orderedAll();

		return view('site.teams', ['pageTitle' => 'Teams', 'teams' => $teams, 'groups' => $groups, 'seasonId' => 1]);
	}
	
	public function team ($teamId)
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		$team    = $this->teamRepository->find($teamId);
		$roster  = $this->teamRepository->roster($teamId);
		$matches = $this->matchRepository->byTeam($teamId);

		if($current_season)
			$stats   = $this->teamStatsRepository->findByTeamSeason($teamId, $current_season->id);
		else
			$stats = [];

		return view('site.team', [
			'pageTitle' => $team->name,
			'team'      => $team,
			'roster'    => $roster,
			'matches'   => $matches,
			'stats'     => $stats
		]);
	}
	
	public function players ()
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		if($current_season)
			$playerStats = $this->playerStatsRepository->findBySeason($current_season->id);
		else
			$playerStats = [];

		return view('site.players', ['pageTitle' => 'Players', 'playerStats' => $playerStats]);
	}
	
	public function player ($playerId)
	{
		$current_season = $this->seasonRepository->currentSeason();
		
		$player          = $this->playerRepository->find($playerId);
		if($current_season)
			$playerStats     = $this->playerStatsRepository->findByPlayerAndSeason($playerId, $current_season->id);
		else
			$playerStats = [];

		$gamePlayerStats = $this->gamePlayerStatsRepository->findByPlayer($playerId);
		
		$service = new PlayerService();
		
		$profile = $service->playerProfileStats($playerStats, $gamePlayerStats);
		
		return view('site.player', [
			'pageTitle' => $player->present()->full_name,
			'player'    => $player,
			'profile'   => $profile,
			'games'     => $gamePlayerStats
		]);
	}
}
