<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\UploadImageJob;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamController extends Controller
{
	/**
	 * @var TeamRepositoryInterface
	 */
	private $repository;
	
	/**
	 * TeamController constructor.
	 *
	 * @param TeamRepositoryInterface $repository
	 */
	public function __construct (TeamRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$teams = $this->repository->orderedAll();
		
		return view('admin.teams.index', ['pageTitle' => 'Manage teams', 'list' => $teams]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.teams.create', ['pageTitle' => 'Create team']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$team = $this->repository->create($request->all());
		
		if ($request->hasFile('logo')) {
			$this->repository->updateLogo($team->id, $request->file('logo')->getClientOriginalName());
			
			$this->dispatch(new UploadImageJob($request->file('logo'), 'team/' . $team->id . '/logo/'));
		}
		
		flash($team->name . ' team was created successfully', 'success');
		
		return redirect()->route('admin.team.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$team = $this->repository->find($id);
		
		return view('admin.teams.delete', ['pageTitle' => 'Delete ' . $team->name, 'team' => $team]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$team   = $this->repository->find($id);
		$roster = $this->repository->roster($id);
		
		return view('admin.teams.edit', ['pageTitle' => 'Edit ' . $team->name, 'team' => $team, 'roster' => $roster]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$team = $this->repository->update($id, $request->all());
		
		if ($request->hasFile('logo')) {
			$this->repository->updateLogo($team->id, $request->file('logo')->getClientOriginalName());
			
			$this->dispatch(new UploadImageJob($request->file('logo'), 'team/' . $team->id . '/logo/'));
		}
		
		flash($team->name . ' team was updated successfully', 'success');
		
		return redirect()->route('admin.team.edit', ['team' => $team->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$this->repository->delete($id);
		
		flash('Team was deleted successfully', 'success');
		
		return redirect()->route('admin.team.index');
	}
}
