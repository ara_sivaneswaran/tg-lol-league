<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Http\Requests\User\StoreRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
	/**
	 * @var UserRepositoryInterface
	 */
	private $repository;
	
	/**
	 * UserController constructor.
	 *
	 * @param UserRepositoryInterface $repository
	 */
	public function __construct (UserRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$users = $this->repository->orderedAll();
		
		return view('admin.users.index', ['pageTitle' => 'Manage users', 'list' => $users]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.users.create', ['pageTitle' => 'Create user']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 *
	 * @param StoreRequest $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (StoreRequest $request)
	{
		$data = $request->all();

		$data['password'] = bcrypt($data['password']);
		
		$user = $this->repository->create($data);
		
		flash($user->name . ' user was created successfully', 'success');
		
		return redirect()->route('admin.user.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$user = $this->repository->find($id);
		
		return view('admin.users.delete', ['pageTitle' => 'Delete ' . $user->name, 'user' => $user]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$user = $this->repository->find($id);
		
		return view('admin.users.edit', ['pageTitle' => 'Edit ' . $user->name, 'user' => $user]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param UpdateRequest $request
	 * @param               $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (UpdateRequest $request, $id)
	{
		$data = $request->all();
		if (is_null($request->input('password')))
			unset($data['password']);
		else
			$data['password'] = bcrypt($data['password']);
		
		$user = $this->repository->update($id, $data);
		
		flash($user->name . ' user was updated successfully', 'success');
		
		return redirect()->route('admin.user.edit', ['user' => $user->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$this->repository->delete($id);
		
		flash('User was deleted successfully', 'success');
		
		return redirect()->route('admin.user.index');
	}
}
