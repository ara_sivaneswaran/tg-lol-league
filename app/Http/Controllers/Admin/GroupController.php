<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Repositories\Interfaces\GroupRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupController extends Controller
{
	/**
	 * @var GroupRepositoryInterface
	 */
	private $repository;
	
	/**
	 * GroupController constructor.
	 *
	 * @param GroupRepositoryInterface $repository
	 */
	public function __construct (GroupRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$groups = $this->repository->orderedAll();
		
		return view('admin.groups.index', ['pageTitle' => 'Manage groups', 'list' => $groups]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.groups.create', ['pageTitle' => 'Create group']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$group = $this->repository->create($request->all());
		
		flash($group->name . ' group was created successfully', 'success');
		
		return redirect()->route('admin.group.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$group = $this->repository->find($id);
		
		return view('admin.groups.delete', ['pageTitle' => 'Delete ' . $group->name, 'group' => $group]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$group = $this->repository->find($id);
		
		return view('admin.groups.edit', ['pageTitle' => 'Edit ' . $group->name, 'group' => $group]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$group = $this->repository->update($id, $request->all());
		
		flash($group->name . ' group was updated successfully', 'success');
		
		return redirect()->route('admin.group.edit', ['group' => $group->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$this->repository->delete($id);
		
		flash('Group was deleted successfully', 'success');
		
		return redirect()->route('admin.group.index');
	}
}
