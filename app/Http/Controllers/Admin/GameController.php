<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\EnterGameResultJob;
use App\Jobs\DeleteGameJob;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Services\GameService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;

class GameController extends Controller
{
	/**
	 * @var GameRepositoryInterface
	 */
	private $repository;
	/**
	 * @var TeamRepositoryInterface
	 */
	private $teamRepository;
	
	
	/**
	 * GameController constructor.
	 *
	 * @param GameRepositoryInterface $repository
	 * @param TeamRepositoryInterface $teamRepository
	 */
	public function __construct (GameRepositoryInterface $repository, TeamRepositoryInterface $teamRepository)
	{
		$this->repository     = $repository;
		$this->teamRepository = $teamRepository;
	}
	
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.games.create', ['pageTitle' => 'Create game']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$game = $this->repository->create($request->all());
		
		flash('Game #' . $game->id . ' was created successfully', 'success');
		
		return redirect()->route('admin.game.edit', ['game' => $game->id]);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$game = $this->repository->find($id);
		
		return view('admin.games.delete', ['pageTitle' => 'Delete ' . $game->name, 'game' => $game]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$game           = $this->repository->find($id);
		$blueTeamRoster = $this->teamRepository->roster($game->blue_team_id);
		$redTeamRoster  = $this->teamRepository->roster($game->red_team_id);
		
		$gameStats   = $this->repository->stats($id);
		$playerStats = $this->repository->playerStats($id);
		
		$service = new GameService();
		
		if($gameStats){
			$gameStats   = $service->formatBansAndPicks($gameStats);
			$playerStats = $service->formatPlayerStats($game->red_team_id, $game->blue_team_id, $playerStats, $gameStats['red']['picks'], $gameStats['blue']['picks']);
			
		}
		
		JavaScript::put([
			'game' => $game
		]);
		
		return view('admin.games.edit', [
			'pageTitle'   => 'Edit ' . $game->name,
			'game'        => $game,
			'blueRoster'  => $blueTeamRoster,
			'redRoster'   => $redTeamRoster,
			'gameStats'   => $gameStats,
			'playerStats' => $playerStats
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$game = $this->repository->find($id);
		
		$this->dispatch(new EnterGameResultJob($game->id, $game->match->season_id, $request->all()));

		$game = $this->repository->update($id, $request->all());
		
		flash('Game #' . $game->id . ' game was updated successfully', 'success');
		
		return redirect()->route('admin.game.edit', ['game' => $game->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$matchId = $this->repository->find($id)->match_id;

		$this->dispatch(new DeleteGameJob($id));
		
		flash('Game was deleted successfully', 'success');
		
		return redirect()->route('admin.match.edit', ['matchId' => $matchId]);
	}
}
