<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\UpdateSeasonManagementJob;
use App\Repositories\Interfaces\GroupRepositoryInterface;
use App\Repositories\Interfaces\SeasonRepositoryInterface;
use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Services\GroupService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;

class SeasonController extends Controller
{
	/**
	 * @var SeasonRepositoryInterface
	 */
	private $repository;
	/**
	 * @var GroupRepositoryInterface
	 */
	private $groupRepository;
	/**
	 * @var TeamRepositoryInterface
	 */
	private $teamRepository;
	
	/**
	 * SeasonController constructor.
	 *
	 * @param SeasonRepositoryInterface $repository
	 * @param GroupRepositoryInterface  $groupRepository
	 * @param TeamRepositoryInterface   $teamRepository
	 */
	public function __construct (SeasonRepositoryInterface $repository, GroupRepositoryInterface $groupRepository, TeamRepositoryInterface $teamRepository)
	{
		$this->repository      = $repository;
		$this->groupRepository = $groupRepository;
		$this->teamRepository  = $teamRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$seasons = $this->repository->orderedAll();
		
		return view('admin.seasons.index', ['pageTitle' => 'Manage seasons', 'list' => $seasons]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.seasons.create', ['pageTitle' => 'Create season']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$season = $this->repository->create($request->all());
		
		flash($season->name . ' season was created successfully', 'success');
		
		return redirect()->route('admin.season.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$season = $this->repository->find($id);
		
		return view('admin.seasons.delete', ['pageTitle' => 'Delete ' . $season->name, 'season' => $season]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 *
	 * @return \Illuminate\Http\Response
	 *
	 */
	public function edit ($id)
	{
		$season = $this->repository->find($id);
		
		$groupsList = $this->groupRepository->orderedAll();
		$teamsList  = $this->teamRepository->orderedAll();
		$service    = new GroupService();
		
		JavaScript::put([
			'groupsList' => $groupsList,
			'teamsList'  => $teamsList,
			'groups'     => $service->groupsWithTeams($season)
		]);
		
		return view('admin.seasons.edit', ['pageTitle' => 'Edit ' . $season->name, 'season' => $season]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$season = $this->repository->update($id, $request->all());
		
		$this->dispatch(new UpdateSeasonManagementJob($season, $request->input('groups')));
		
		flash($season->name . ' season was updated successfully', 'success');
		
		return redirect()->route('admin.season.edit', ['season' => $season->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$season = $this->repository->delete($id);
		
		flash($season->name . ' season was deleted successfully', 'success');
		
		return redirect()->route('admin.season.index');
	}
}
