<?php

namespace App\Http\Controllers\Admin;

use App\Player;
use App\Repositories\Interfaces\PlayerRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlayerController extends Controller
{
	/**
	 * @var PlayerRepositoryInterface
	 */
	private $repository;
	
	/**
	 * PlayerController constructor.
	 *
	 * @param PlayerRepositoryInterface $repository
	 */
	public function __construct (PlayerRepositoryInterface $repository)
	{
		$this->repository = $repository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		
		$players = $this->repository->orderedAll();
		
		return view('admin.players.index', ['pageTitle' => 'Manage players', 'list' => $players]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.players.create', ['pageTitle' => 'Create player']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$player = $this->repository->create($request->all());
		
		if ($request->hasFile('photo')) {
			$this->repository->updatePhoto($player->id, $request->file('photo')->getClientOriginalName());
			
			$this->dispatch(new UploadImageJob($request->file('photo'), 'player/' . $player->id . '/photo/'));
		}
		
		flash($player->present()->full_name . ' player was created successfully', 'success');
		
		return redirect()->route('admin.player.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$player = $this->repository->find($id);
		
		return view('admin.players.delete', ['pageTitle' => 'Delete ' . $player->name, 'player' => $player]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$player = $this->repository->find($id);
		
		return view('admin.players.edit', ['pageTitle' => 'Edit ' . $player->name, 'player' => $player]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$player = $this->repository->update($id, $request->all());
		
		if ($request->hasFile('photo')) {
			$this->repository->updatePhoto($player->id, $request->file('photo')->getClientOriginalName());
			
			$this->dispatch(new UploadImageJob($request->file('photo'), 'player/' . $player->id . '/photo/'));
		}
		
		flash($player->present()->full_name . ' player was updated successfully', 'success');
		
		return redirect()->route('admin.player.edit', ['player' => $player->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$this->repository->delete($id);
		
		flash('Player was deleted successfully', 'success');
		
		return redirect()->route('admin.player.index');
	}
}
