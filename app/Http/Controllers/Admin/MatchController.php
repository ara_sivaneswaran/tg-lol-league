<?php

namespace App\Http\Controllers\Admin;

use App\Match;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MatchController extends Controller
{
	/**
	 * @var MatchRepositoryInterface
	 */
	private $repository;
	/**
	 * @var GameRepositoryInterface
	 */
	private $gameRepository;
	
	/**
	 * MatchController constructor.
	 *
	 * @param MatchRepositoryInterface $repository
	 * @param GameRepositoryInterface  $gameRepository
	 */
	public function __construct (MatchRepositoryInterface $repository, GameRepositoryInterface $gameRepository)
	{
		$this->repository = $repository;
		$this->gameRepository = $gameRepository;
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index ()
	{
		$matches = $this->repository->orderedAll('desc');
		
		return view('admin.matches.index', ['pageTitle' => 'Manage matches', 'list' => $matches]);
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create ()
	{
		return view('admin.matches.create', ['pageTitle' => 'Create match']);
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		$match = $this->repository->create($request->all());
		
		flash('Match #' . $match->id . ' was created successfully', 'success');
		
		return redirect()->route('admin.match.create');
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show ($id)
	{
		$match = $this->repository->find($id);
		
		return view('admin.matches.delete', ['pageTitle' => 'Delete ' . $match->name, 'match' => $match]);
	}
	
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($id)
	{
		$match = $this->repository->find($id);
		$games = $this->gameRepository->byMatch($id);
		
		return view('admin.matches.edit', ['pageTitle' => 'Edit ' . $match->name, 'match' => $match, 'games' => $games]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param                           $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $id)
	{
		$match = $this->repository->update($id, $request->all());
		
		flash('Match #' . $match->id . ' match was updated successfully', 'success');
		
		return redirect()->route('admin.match.edit', ['match' => $match->id]);
	}
	
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($id)
	{
		$this->repository->delete($id);
		
		flash($match->name . ' match was deleted successfully', 'success');
		
		return redirect()->route('admin.match.index');
	}
	
	public function getCreateGame ($id)
	{
		$match = $this->repository->find($id);
		
		return view('admin.games.create', ['pageTitle' => 'Create game', 'match' => $match]);
	}
}
