<?php

namespace App\Presenters;

use App\Services\AccessorService;
use Laracasts\Presenter\Presenter;

class PlayerPresenter extends Presenter
{
	
	public function full_name ()
	{
		return $this->first_name . ' "' . $this->summoner_name . '" ' . $this->last_name;
	}
	
	public function photoUrl ()
	{
		if(is_null($this->photo))
			return 'player/no-photo.jpg';
		return 'player/' . $this->id . '/photo/' . $this->photo;
	}
	
	public function url ()
	{
		return '<a href="' . route('player', ['playerId' => $this->id]) . '">' . $this->summoner_name . '</a>';
	}

	public function primaryRole ()
	{
		$service = new AccessorService();
		$roles = $service->getRoles();

		return $roles[$this->primary_role];
	}

	public function secondaryRole ()
	{
		$service = new AccessorService();
		$roles = $service->getRoles();

		return $roles[$this->secondary_role];
	}
	
	public function roles ()
	{
		return '<strong>' . $this->primaryRole() . '</strong> / ' . $this->secondaryRole();
	}
	
	public function teamName ()
	{
		if($this->team)
			return $this->team->name;
		else
			return "Not in a team";
	}
	
	public function teamLogo ()
	{
		if($this->team)
			return $this->team->present()->imageUrl;
		else
			return 'team/no-logo.png';
	}
}