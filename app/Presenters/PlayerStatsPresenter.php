<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class PlayerStatsPresenter extends Presenter
{
	
	public function formatStats ()
	{
		return $this->kills . ' /' . $this->deaths . '/' . $this->assists;
	}
	
	public function kda ()
	{
		if($this->deaths > 0)
			return round(($this->kills + $this->assists)/$this->deaths,2);
		else
			return 'Perfect';
	}

	public function kpPct()
	{
		return $this->kp . '%';
	}

	public function avgDmg()
	{
		return @round($this->damage/$this->gp);
	}
}