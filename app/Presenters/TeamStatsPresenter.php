<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class TeamStatsPresenter extends Presenter
{
	
	public function record ()
	{
		return $this->wins . 'W - ' . $this->loss . 'L';
	}
	
	public function winPct ()
	{
		return @round($this->wins/$this->gp, 2);
	}
}