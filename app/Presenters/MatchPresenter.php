<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class MatchPresenter extends Presenter
{
	
	public function formattedDate ()
	{
		return $this->date->format('Y-m-d');
	}

	public function readableDate ()
	{
		return $this->date->format('l M d, Y');
	}
	
	public function humanDate ()
	{
		return $this->date->diffForHumans();
	}
	
	public function seasonName ()
	{
		return $this->season->name;
	}
	
	public function team1Name ()
	{
		return $this->team1->name;
	}
	
	public function team1Url ()
	{
		return '<a href="' . route('team', ['teamId' => $this->team1_id]) . '">' . $this->team1->name . '</a>';
	}
	
	public function team2Name ()
	{
		return $this->team2->name;
	}
	
	public function team2Url ()
	{
		return '<a href="' . route('team', ['teamId' => $this->team2_id]) . '">' . $this->team2->name . '</a>';
	}
	
	public function url ()
	{
		if($this->finished)
			return '<a href="' .route('match', ['matchId' => $this->id ]). '">View result</a>';
		else
			return '<a href="' .route('match', ['matchId' => $this->id ]). '">View Pre-game</a>';
	}

	public function formatTime()
	{
		return date ('H:i',strtotime($this->time));
	}

	public function formatType()
	{
		if($this->type === 'regular')
			return 'Regular season';
		else if($this->type === 'playoffs')
			return 'Playoffs';
		else
			return 'Finals';
	}

	public function bestOf()
	{
		return 'Best of ' . $this->best_of;
	}
	
	public function resultUrl ()
	{
		if ($this->finished)
			return '<a href="' . route('match', ['matchId' => $this->id]) . '">' . $this->team1_score . ' - ' . $this->team2_score . '</a>';
		else
			return '<a href="' . route('match', ['matchId' => $this->id]) . '">View Pre-game</a>';
	}
	
	public function winnerId ()
	{
		if($this->team1_score > $this->team2_score)
			return $this->team1_id;
		else
			return $this->team2_id;
	}
}