<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class TeamPresenter extends Presenter
{
	
	public function imageUrl ()
	{
		if (is_null($this->logo))
			return 'team/no-logo.png';
		return 'team/' . $this->id . '/logo/' . $this->logo;
	}
	
	public function url ()
	{
		return '<a href="' . route('team', ['teamId' => $this->id]) . '">' . $this->name . '</a>';
	}
	
}