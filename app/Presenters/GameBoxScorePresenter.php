<?php

namespace App\Presenters;

use App\Services\AccessorService;
use Laracasts\Presenter\Presenter;

class GameBoxScorePresenter extends Presenter
{
	
	public function formatStats ()
	{
		return $this->kills . ' /' . $this->deaths . '/ ' . $this->assists;
	}

}