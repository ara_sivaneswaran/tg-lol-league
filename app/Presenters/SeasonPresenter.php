<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class SeasonPresenter extends Presenter
{
	
	public function startDate ()
	{
		return $this->start_date->format('Y-m-d');
	}
	
	public function endDate ()
	{
		return $this->end_date->format('Y-m-d');
	}
	
}