<?php

namespace App\Presenters;

use Laracasts\Presenter\Presenter;

class ChampionPresenter extends Presenter
{
	
	public function photo ()
	{
		return '<img src="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/' . $this->image . '"
										alt="' . $this->name . '" style="width: 75px">';
	}

	public function photoSmall()
	{
		return '<img src="http://ddragon.leagueoflegends.com/cdn/6.24.1/img/champion/' . $this->image . '"
										alt="' . $this->name . '" style="width: 40px">';
	}

}