<?php

namespace App\Presenters;

use App\Services\AccessorService;
use Laracasts\Presenter\Presenter;

class GamePlayerStatsPresenter extends Presenter
{
	
	public function opponentName ()
	{
		if($this->game->blue_team_id == $this->team_id)
			return $this->game->red_team->name;
		else
			return $this->game->blue_team->name;
	}
	
	public function formatStats ()
	{
		return $this->kills . ' /' . $this->deaths . '/ ' . $this->assists;
	}
	
	public function kda ()
	{
		if($this->deaths > 0)
			return round(($this->kills + $this->assists) / $this->deaths, 2);
		else
			return 'Perfect';
	}

	public function kpPct()
	{
		return $this->kp . '%';
	}
	
	public function champResultIcon ()
	{
		if($this->game->winner == $this->team_id)
			return '<span class="icon game-result text-success">W</span>';
		else
			return '<span class="icon game-result text-danger">L</span>';
	}

	public function roleF ()
	{
		$service = new AccessorService();
		$roles = $service->getRoles();

		return $roles[$this->role];
	}
}