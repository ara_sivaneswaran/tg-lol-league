<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class TeamStats extends Model
{
	use SoftDeletes;
	use PresentableTrait;

	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\TeamStatsPresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'team_stats';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'team_id',
		'season_id',
		'gp',
		'wins',
		'loss',
		'games_won',
		'games_lost',
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	/**
	 * Model's relationships
	 */
	public function team ()
	{
		return $this->belongsTo(Team::class);
	}
}