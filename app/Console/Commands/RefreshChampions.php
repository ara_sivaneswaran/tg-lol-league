<?php

namespace App\Console\Commands;

use App\Jobs\FetchChampionsDataJob;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

class RefreshChampions extends Command
{
	use DispatchesJobs;
	
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:champions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch champions data from Riot using their API.';

	
	/**
	 * Create a new command instance.
	 *
	 */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dispatch(new FetchChampionsDataJob());
    }
}
