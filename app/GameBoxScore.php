<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class GameBoxScore extends Model
{
	use SoftDeletes;
		use PresentableTrait;
	
	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\GameBoxScorePresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'games_box_scores';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'game_id',
		'team_id',
		'kills',
		'assists',
		'deaths',
		'damage',
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	public function team()
	{
		return $this->belongsTo(Team::class);
	}
	
}