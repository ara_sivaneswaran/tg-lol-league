<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class Team extends Model
{
	use SoftDeletes;
	use PresentableTrait;
	
	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\TeamPresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'teams';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'logo', 'description', 'created_at', 'updated_at'];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at'];
	
	/**
	 * Model's relationships
	 */
	public function groups ()
	{
		return $this->belongsToMany(Group::class, 'groups_teams', 'team_id', 'group_id');
	}
	
	public function players ()
	{
		return $this->hasMany(Player::class);
	}
	
	public function seasons ()
	{
		return $this->belongsToMany(Season::class, 'teams_seasons', 'team_id', 'season_id');
	}
	
	public function stats ()
	{
		return $this->hasMany(TeamStat::class);
	}

	public function scopeGroupsBySeason($query, $seasonId)
	{
		return $this->groups()->whereHas('seasons', function ($query) use($seasonId) {
		    $query->where('seasons.id', $seasonId);
		})->first();
	}
}