<?php

namespace App\Services;


class PlayerService
{
	
	/**
	 * @param     $playerStats
	 * @param     $playerGameStats
	 * @param int $take
	 *
	 * @return mixed
	 */
	public function playerProfileStats ($playerStats, $playerGameStats, $take = 3)
	{
		if(count($playerStats) > 0) {
			$stats = [
				'kills'   => $playerStats->sum('kills'),
				'deaths'  => $playerStats->sum('deaths'),
				'assists' => $playerStats->sum('assists'),
				'damage'  => round($playerStats->sum('damage') / $playerStats->sum('gp')),
				'kp'  => $playerStats->avg('kp'),
			];
		} else {
			$stats = [
				'kills'   => 0,
				'deaths'  => 0,
				'assists' => 0,
				'damage'  => 0,
				'kp'      => 0,
			];
		}
		
		$stats['kda'] = @stats['deaths'] != 0 ? @round(($stats['kills'] + $stats['assists']) / $stats['deaths'], 2) : 'Perfect';
		
		$champions = $playerGameStats->groupBy('champion');
		
		$champStats = [];
		
		foreach ($champions as $id => $champion) {
			$champStats[$id] = [
				'gp'      => 0,
				'kills'   => 0,
				'assists' => 0,
				'deaths'  => 0,
				'dmg'     => 0,
				'wins'    => 0,
				'loss'    => 0,
				'damage'  => 0,
			];
			$teamKills = 0;

			foreach ($champion as $game) {
				$champStats[$id]['gp']++;
				$champStats[$id]['name'] = $game->championM->name;
				$champStats[$id]['photo'] = $game->championM->image;

				$teamKills += $game->game->boxScores()->where('team_id', $game->team_id)->first()->kills;

				if ($game->game->winner == $game->team_id)
					$champStats[$id]['wins']++;
				else
					$champStats[$id]['loss']++;
				
				$champStats[$id]['kills'] += $game->kills;
				$champStats[$id]['deaths'] += $game->deaths;
				$champStats[$id]['assists'] += $game->assists;
				$champStats[$id]['damage'] = round(($game->damage + $champStats[$id]['damage']) / $champStats[$id]['gp']);
				$champStats[$id]['kda'] = $champStats[$id]['deaths'] != 0 ? round(($champStats[$id]['kills'] + $champStats[$id]['assists']) / $champStats[$id]['deaths'], 2) : "Perfect";
			}

			$champStats[$id]['kp'] = round((($champStats[$id]['kills'] + $champStats[$id]['assists'])/$teamKills)*100);
		}
		
		$champStats = collect($champStats);
		$champStats = $champStats->sortByDesc('gp');
		$champStats = $champStats->take($take);
		
		$profile['stats']      = $stats;
		$profile['champStats'] = $champStats;
		
		return $profile;
	}
}