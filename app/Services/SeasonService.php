<?php

namespace App\Services;

use App\Season;
use Carbon\Carbon;

class SeasonService
{
	
	public function getCurrentSeason ()
	{
		$now = Carbon::now()->format('Y-m-d');
		
		$season = Season::where('start_date', '<=', $now)->where('end_date', '>=', $now)->orderBy('start_date', 'desc')->first();

		if(is_null($season))
		    $season = Season::where('start_date', '<', $now)->orderBy('start_date', 'desc')->first();

        if(is_null($season))
            $season = Season::orderBy('start_date', 'desc')->first();

		return $season;
	}
}