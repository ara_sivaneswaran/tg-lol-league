<?php

namespace App\Services;

class AccessorService
{
	public function getRoles ()
	{
		$roles = [
			'top' => 'Top',
		    'jng' => 'Jungle',
		    'mid' => 'Middle',
		    'adc' => 'AD Carry',
		    'sup' => 'Support'
		];
		
		return $roles;
	}

	public function getMatchTypes ()
	{
		$types = [
			'regular' => 'Regular',
			'playoffs' => 'Playoffs',
			'finals' => 'Finals',
		];

		return $types;
	}
}
	