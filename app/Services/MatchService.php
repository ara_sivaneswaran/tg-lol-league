<?php

namespace App\Services;

use App\Champion;

class MatchService
{
	public function formatBansAndPicks($games)
	{
		$array = [];

		foreach($games as $game)
		{
			$stats = $game->stats;

			if($stats)
			{
				for ($i = 1; $i <= 5; $i++) {
					$redBanVar  = 'red_ban_' . $i;
					$redPickVar = 'red_pick_' . $i;
					
					$blueBanVar  = 'blue_ban_' . $i;
					$bluePickVar = 'blue_pick_' . $i;

					$redBanChamp = Champion::find($stats->$redBanVar);
					$redPickChamp = Champion::find($stats->$redPickVar);

					$blueBanChamp = Champion::find($stats->$blueBanVar);
					$bluePickChamp = Champion::find($stats->$bluePickVar);

					if($redBanChamp)
						$array[$game->id]['red']['bans'][$i] = $redBanChamp->present()->photoSmall;

					$array[$game->id]['red']['picks'][$i] = $redPickChamp->present()->photoSmall;

					if($blueBanChamp)
						$array[$game->id]['blue']['bans'][$i] = $blueBanChamp->present()->photoSmall;

					$array[$game->id]['blue']['picks'][$i] = $bluePickChamp->present()->photoSmall;
				}
			}
		}

		return $array;
	}

	public function formatPlayerStats($games)
	{
		$array = [];

		foreach($games as $game)
		{
			foreach($game->playerStats as $playerStats)
			{
				if($playerStats->team_id == $game->blue_team_id)
					$color = 'blue';
				else
					$color = 'red';

				$data = [
					'id' => $playerStats->player_id,
					'role' => $playerStats->present()->roleF,
					'champion' => $playerStats->championM->present()->photoSmall,
					'name' => $playerStats->player->summoner_name,
					'stats' => $playerStats->present()->formatStats,
					'kda' => $playerStats->present()->kda,
					'kp' => $playerStats->present()->kpPct,
					'damage' => $playerStats->damage,

				];

				$array[$game->id][$color][$playerStats->role] = $data;
			}
		}

		return $array;
	}
}
	