<?php

namespace App\Services;

use App\Champion;
use App\Match;
use App\Season;
use App\Team;

class FormService
{
	protected $accessorService;
	
	public function __construct ()
	{
		$this->accessorService = new AccessorService();
	}
	
	public function rolesArray ()
	{
		return $this->accessorService->getRoles();
	}

	public function matchTypesArray ()
	{
		return $this->accessorService->getMatchTypes();
	}
	
	public function teamsArray ()
	{
		$array = [];
		
		foreach(Team::orderBy('name', 'asc')->get() as $team)
			$array[$team->id] = $team->name;
		
		return $array;
	}
	
	public function seasonsArray ()
	{
		$array = [];
		
		foreach (Season::orderBy('name', 'asc')->get() as $season)
			$array[$season->id] = $season->name;
		
		return $array;
	}
	
	public function teamsFromMatchArray ($matchId)
	{
		$array = [];
		$match = Match::find($matchId);

		$array[$match->team1->id] = $match->team1->name;
		$array[$match->team2->id] = $match->team2->name;
		
		return $array;
	}
	
	public function championsArray ()
	{
		$array = [];
		
		foreach (Champion::orderBy('name', 'asc')->get() as $champion)
			$array[$champion->id] = $champion->name;
		
		return $array;
	}
}
	