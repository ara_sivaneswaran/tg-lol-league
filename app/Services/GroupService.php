<?php

namespace App\Services;

use App\Group;
use App\Season;

class GroupService
{
	private $seasonService;
	
	/**
	 * GroupService constructor.
	 */
	public function __construct ()
	{
		$this->seasonService = new SeasonService();
	}
	
	/**
	 * @param Season|null $season
	 *
	 * @return array
	 */
	public function groupsWithTeams (Season $season = null)
	{
		if(is_null($season))
			$season = $this->seasonService->getCurrentSeason();
		
		$groups = Group::has('seasons', $season->id)->with('teams')->orderBy('name', 'asc')->get();
		
		$array = [];
		
		foreach ($groups as $group) {
			$teams = [];
			foreach ($group->teams()->has('seasons', $season->id)->orderBy('name', 'asc')->get() as $team) {
				$teams[] = [
					'id' => $team->id,
				    'name' => $team->name
				];
			}
			
			$array[] = [
				'id'    => $group->id,
				'name'  => $group->name,
				'teams' => $teams
			];
		}
		
		return $array;
	}
}