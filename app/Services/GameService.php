<?php

namespace App\Services;

use App\Champion;

class GameService
{
	public function formatBansAndPicks ($stats)
	{
		$array = [];
		
		if($stats){
			for ($i = 1; $i <= 5; $i++) {
				$redBanVar  = 'red_ban_' . $i;
				$redPickVar = 'red_pick_' . $i;
				
				$blueBanVar  = 'blue_ban_' . $i;
				$bluePickVar = 'blue_pick_' . $i;
				
				$array['red']['bans'][$i]  = $stats->$redBanVar;
				$array['red']['picks'][$i] = $stats->$redPickVar;
				
				$array['blue']['bans'][$i]  = $stats->$blueBanVar;
				$array['blue']['picks'][$i] = $stats->$bluePickVar;
			}
		}
		
		return $array;
	}
	
	public function formatPlayerStats ($redTeamId, $blueTeamId, $stats, $redPicks, $bluePicks)
	{
		$array = [];
		
		$red_counter  = 0;
		$blue_counter = 0;
		foreach ($stats as $player) {
			if ($redTeamId == $player->team_id) {
				$array['red'][$red_counter]['pick_order'] = array_search($player->champion, $redPicks);
				$array['red'][$red_counter]['champion']   = $player->champion;
				$array['red'][$red_counter]['player_id']  = $player->player_id;
				$array['red'][$red_counter]['role']       = $player->role;
				$array['red'][$red_counter]['kills']      = $player->kills;
				$array['red'][$red_counter]['assists']    = $player->assists;
				$array['red'][$red_counter]['deaths']     = $player->deaths;
				$array['red'][$red_counter]['damage']     = $player->damage;
				
				$red_counter++;
			}
			else if ($blueTeamId == $player->team_id) {
				$array['blue'][$blue_counter]['pick_order'] = array_search($player->champion, $bluePicks);
				$array['blue'][$blue_counter]['champion']   = $player->champion;
				$array['blue'][$blue_counter]['player_id']  = $player->player_id;
				$array['blue'][$blue_counter]['role']       = $player->role;
				$array['blue'][$blue_counter]['kills']      = $player->kills;
				$array['blue'][$blue_counter]['assists']    = $player->assists;
				$array['blue'][$blue_counter]['deaths']     = $player->deaths;
				$array['blue'][$blue_counter]['damage']     = $player->damage;
				
				$blue_counter++;
			}
		}
		
		return $array;
	}

	public function showBansAndPicks($stats)
	{
		if($stats){
			for ($i = 1; $i <= 5; $i++) {
				$redBanVar  = 'red_ban_' . $i;
				$redPickVar = 'red_pick_' . $i;
				
				$blueBanVar  = 'blue_ban_' . $i;
				$bluePickVar = 'blue_pick_' . $i;

				$redBanChamp = Champion::find($stats->$redBanVar);
				$redPickChamp = Champion::find($stats->$redPickVar);

				$blueBanChamp = Champion::find($stats->$blueBanVar);
				$bluePickChamp = Champion::find($stats->$bluePickVar);
				
				$array['red']['bans'][$i]  = $redBanChamp->present()->image;
				$array['red']['picks'][$i] = $redPickChamp->present()->image;
				
				$array['blue']['bans'][$i]  = $blueBanChamp->present()->image;
				$array['blue']['picks'][$i] = $bluePickChamp->present()->image;
			}
		}
	}
}
	