<?php

namespace App\Services;

use App\Season;
use Carbon\Carbon;

class StandingsService
{
	protected $teamRepository;
	protected $teamStatsRepository;
	protected $groupRepository;
	protected $seasonRepository;
	
	public function __construct ()
	{
		$this->teamRepository      = resolve('App\Repositories\Interfaces\TeamRepositoryInterface');
		$this->teamStatsRepository = resolve('App\Repositories\Interfaces\TeamStatsRepositoryInterface');
		$this->groupRepository     = resolve('App\Repositories\Interfaces\GroupRepositoryInterface');
		$this->seasonRepository    = resolve('App\Repositories\Interfaces\SeasonRepositoryInterface');
	}
	
	private function createStandings ($seasonId)
	{
		$standings = [];
		
		$groups = $this->seasonRepository->groups($seasonId);
		
		foreach ($groups as $group) {
			$teams = $this->groupRepository->teams($group->id);
			
			foreach ($teams as $team) {
				$teamStats = $this->teamStatsRepository->findByTeamSeason($team->id, $seasonId);
				
				if ($teamStats) {
					$stats = [
						'team_id'    => $team->id,
						'team'       => $team->name,
						'group_id'   => $group->id,
						'imageUrl'   => $team->present()->imageUrl,
						'gp'         => $teamStats->gp,
						'wins'       => $teamStats->wins,
						'loss'       => $teamStats->loss,
						'games_won'  => $teamStats->games_won,
						'games_lost' => $teamStats->games_lost
					];
					
					$standings[] = $stats;
				}
			}
		}
		
		$standings = collect($standings);
		$standings->sortByDesc('wins')->sortByDesc('games_won')->sortBy('loss')->sortBy('games_lost');
		
		return $standings;
	}
	
	public function standings ($seasonId)
	{
		return self::createStandings($seasonId);
	}
	
	public function groupStandings ($seasonId, $groupId)
	{
		$standings = self::createStandings($seasonId);
		
		return $standings->where('group_id', $groupId);
	}
}