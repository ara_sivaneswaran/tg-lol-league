<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laracasts\Presenter\PresentableTrait;

class GamePlayerStats extends Model
{
	use SoftDeletes;
	use PresentableTrait;
	
	/**
	 * The presenter class used to display data of this model.
	 *
	 * @var string
	 */
	protected $presenter = "App\Presenters\GamePlayerStatsPresenter";
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'game_player_stats';
	
	/**
	 * Attributes that should be mass-assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'game_id',
		'team_id',
		'role',
		'champion',
		'player_id',
		'kills',
		'assists',
		'deaths',
		'damage',
		'kp',
		'created_at',
		'updated_at',
		'deleted_at'
	];
	
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];
	
	/**
	 * The attributes that should be casted to native types.
	 *
	 * @var array
	 */
	protected $casts = [];
	
	/**
	 * The attributes that should be mutated to dates.
	 *
	 * @var array
	 */
	protected $dates = ['created_at', 'updated_at', 'deleted_at'];
	
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function game ()
	{
		return $this->belongsTo(Game::class);
	}
	
	public function championM ()
	{
		return $this->hasOne(Champion::class, 'id', 'champion');
	}

	public function player ()
	{
		return $this->belongsTo(Player::class);
	}
}