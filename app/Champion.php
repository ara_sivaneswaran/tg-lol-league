<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;

class Champion extends Model  {
    use PresentableTrait;
    
    /**
     * The presenter class used to display data of this model.
     *
     * @var string
     */
    protected $presenter = "App\Presenters\ChampionPresenter";

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'champions';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['champion_id', 'name', 'title', 'image', 'created_at', 'updated_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

}