<?php

namespace App\Jobs;

use App\Group;
use App\Season;

class UpdateSeasonManagementJob {
	
	/**
	 * @var Season
	 */
	private $season;
	
	private $data;
	
	/**
	 * Create a new job instance.
	 *
	 * @param Season $season
	 * @param        $data
	 */
	public function __construct (Season $season, $data)
	{
		$this->season = $season;
		$this->data = $data;
	}
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle ()
	{
		$group_ids = [];
		$team_ids = [];
		
		$data = json_decode($this->data);
		
		foreach($data as $group) {
			$group_ids[] = $group->id;
			$groupModel = Group::find($group->id);
			
			$groupModel->teams()->detach();
			
			foreach($group->teams as $team) {
				$team_ids[] = $team->id;
				
				$groupModel->teams()->attach($team->id);
			}
		}
		
		$this->season->groups()->sync($group_ids);
		$this->season->teams()->sync($team_ids);
	}
}