<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\PlayerStatsRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdatePlayerStatsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $seasonId;
	/**
	 * @var
	 */
	private $teamId;
	/**
	 * @var
	 */
	private $stats;
	
	/**
	 * Create a new job instance.
	 *
	 * @param $seasonId
	 * @param $teamId
	 * @param $stats
	 */
	public function __construct ($seasonId, $teamId, $stats)
	{
		$this->seasonId = $seasonId;
		$this->teamId   = $teamId;
		$this->stats    = $stats;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param PlayerStatsRepositoryInterface     $repository
	 * @param GamePlayerStatsRepositoryInterface $gamePlayerStatsRepository
	 */
	public function handle (PlayerStatsRepositoryInterface $repository, GamePlayerStatsRepositoryInterface $gamePlayerStatsRepository)
	{	
		foreach ($this->stats as $player) {
			$data = [
				'team_id'    => $this->teamId,
				'season_id'  => $this->seasonId,
				'gp' => 0,
				'kills'      => 0,
				'assists'    => 0,
				'deaths'     => 0,
				'damage'     => 0,
				'kp'         => 0,
			];

			$playerStats = $repository->findByPlayerTeamAndSeason($player['player_id'], $this->teamId, $this->seasonId);
			
			$data['player_id'] = $player['player_id'];
			
			if (is_null($playerStats))
				$playerStats = $repository->create($data);

			$teamKills = 0;
			
			// Update player stats
			$games = $gamePlayerStatsRepository->findByPlayer($player['player_id']);
			foreach ($games as $game) {
				$data['gp'] += 1;
				$data['kills']   += $game->kills;
				$data['assists'] += $game->assists;
				$data['deaths']  += $game->deaths;
				$data['damage']  += $game->damage;
				$data['kp']      += $game->kp;
			}

			$data['kp'] = round($data['kp'] / $data['gp']);
			
			$repository->update($playerStats->id, $data);
		}
	}
}
