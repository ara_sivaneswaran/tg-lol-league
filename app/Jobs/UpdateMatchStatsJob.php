<?php

namespace App\Jobs;

use App\Match;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateMatchStatsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var Match
	 */
	private $match;
	
	/**
	 * @var winner
	 */
	private $winner;
	/**
	 * @var
	 */
	private $finished;
	/**
	 * @var
	 */
	private $winnerChanged;
	
	/**
	 * Create a new job instance.
	 *
	 * @param Match $match
	 * @param       $winner
	 * @param       $finished
	 */
	public function __construct (Match $match, $winner, $finished, $winnerChanged)
	{
		$this->match  = $match;
		$this->winner = $winner;
		$this->finished = $finished;
		$this->winnerChanged = $winnerChanged;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param MatchRepositoryInterface $repository
	 */
	public function handle (MatchRepositoryInterface $repository)
	{
		$data = [
			'team1_score' => $this->match->team1_score,
			'team2_score' => $this->match->team2_score,
			'finished'    => $this->match->finished
		];
		
		if($this->finished)
		{
			if ($this->winnerChanged) {
				if ($this->winner == $this->match->team1_id)
					$data['team2_score']--;
				else
					$data['team1_score']--;
			}
			else {
				if ($this->winner == $this->match->team1_id)
					$data['team1_score']--;
				else
					$data['team2_score']--;
			}
		}
		
		if ($this->winner == $this->match->team1_id)
			$data['team1_score']++;
		else
			$data['team2_score']++;

		if (($data['team1_score'] / $this->match->best_of) >= 0.5  || ($data['team2_score'] / $this->match->best_of) >= 0.5 )
			$data['finished'] = true;
		
		$repository->update($this->match->id, $data);
	}
}
