<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameStatsRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateGameStatsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $bans;
	/**
	 * @var
	 */
	private $picks;
	/**
	 * @var
	 */
	private $blue_picks;
	/**
	 * @var
	 */
	private $red_picks;
	
	/**
	 * Create a new job instance.
	 *
	 * @param $gameId
	 * @param $bans
	 * @param $blue_picks
	 * @param $red_picks
	 *
	 * @internal param $picks
	 */
    public function __construct($gameId, $bans, $blue_picks, $red_picks)
    {
	    $this->gameId = $gameId;
	    $this->bans = $bans;
	    $this->blue_picks = $blue_picks;
	    $this->red_picks = $red_picks;
    }
	
	/**
	 * Execute the job.
	 *
	 * @param GameStatsRepositoryInterface $repository
	 */
    public function handle(GameStatsRepositoryInterface $repository)
    {
        $gameStats = $repository->findByGame($this->gameId);
	    
	    $data = [];
	    
	    $data['game_id'] = $this->gameId;
	    
	    foreach($this->bans as $side => $bans){
		    $side = str_replace("'", "", $side);
		    foreach($bans as $index => $ban){
			    $data[$side . '_ban_' . $index] = $ban;
		    }
	    }

	    foreach($this->blue_picks as $pick)
		    $data['blue_pick_' . $pick['pick_order']] = $pick['champion'];
	
	    foreach ($this->red_picks as $pick)
		    $data['red_pick_' . $pick['pick_order']] = $pick['champion'];
	    
	    if(is_null($gameStats))
	    	$repository->create($data);
	    else
	    	$repository->update($gameStats->id, $data);
    }
}
