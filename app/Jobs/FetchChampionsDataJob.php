<?php

namespace App\Jobs;

use App\Champion;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FetchChampionsDataJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	
	
	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle ()
	{
		$url     = 'http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json';
		$client   = new Client();
		$response = $client->get($url);
		$data     = json_decode($response->getBody()->getContents());
		
		foreach ($data->data as $champion) {
			$infos = ['champion_id' => $champion->key, 'name' => $champion->name, 'title' => $champion->title, 'image' => $champion->image->full];
			Champion::firstOrCreate($infos);
	    }
	}
}
