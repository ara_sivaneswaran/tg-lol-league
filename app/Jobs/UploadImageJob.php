<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadImageJob
{
    use Dispatchable, Queueable, SerializesModels;
	/**
	 * @var UploadedFile
	 */
	private $image;
	private $path;
	
	/**
	 * Create a new job instance.
	 *
	 * @param UploadedFile $image
	 * @param              $path
	 */
    public function __construct(UploadedFile $image, $path)
    {
	    $this->image = $image;
	    $this->path = $path;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
	    $filename = $this->image->getClientOriginalName();
        $this->image->storeAs($this->path, $filename, 's3');
    }
}
