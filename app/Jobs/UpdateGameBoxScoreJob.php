<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateGameBoxScoreJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $teamId;
	/**
	 * @var
	 */
	private $team_stats;
	
	
	/**
	 * Create a new job instance.
	 *
	 * @param $gameId
	 * @param $teamId
	 * @param $team_stats
	 */
	public function __construct ($gameId, $teamId, $team_stats)
	{
		$this->gameId     = $gameId;
		$this->teamId     = $teamId;
		$this->team_stats = $team_stats;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param GameBoxScoreRepositoryInterface $repository
	 */
	public function handle (GameBoxScoreRepositoryInterface $repository)
	{
		$boxScore = $repository->findByGameAndTeam($this->gameId, $this->teamId);
		
		$team_data = [
			'game_id' => $this->gameId,
			'team_id' => $this->teamId,
			'kills'   => 0,
			'deaths'  => 0,
			'assists' => 0,
			'damage'    => 0
		];
		
		foreach ($this->team_stats as $stats) {
			$team_data['kills'] += $stats['kills'];
			$team_data['assists'] += $stats['assists'];
			$team_data['deaths'] += $stats['deaths'];
			$team_data['damage'] += $stats['damage'];
		}
		
		if (is_null($boxScore))
			$repository->create($team_data);
		else
			$repository->update($boxScore->id, $team_data);
	}
}
