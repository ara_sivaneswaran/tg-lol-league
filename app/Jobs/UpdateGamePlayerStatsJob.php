<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateGamePlayerStatsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $teamId;
	/**
	 * @var
	 */
	private $roster;
	
	/**
	 * Create a new job instance.
	 *
	 * @param $gameId
	 * @param $teamId
	 * @param $roster
	 */
	public function __construct ($gameId, $teamId, $roster)
	{
		$this->gameId = $gameId;
		$this->teamId = $teamId;
		$this->roster = $roster;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param GamePlayerStatsRepositoryInterface $repository
	 */
	public function handle (GamePlayerStatsRepositoryInterface $repository, GameBoxScoreRepositoryInterface $gameBoxScoreRepository)
	{
		$teamKills = $gameStats = $gameBoxScoreRepository->findByGameAndTeam($this->gameId, $this->teamId)->kills;
		
		foreach ($this->roster as $player) {
			$playerStats = $repository->findByGameTeamAndPlayer($this->gameId, $this->teamId, $player['player_id']);
			
			$data = [
				'game_id'   => $this->gameId,
				'team_id'   => $this->teamId,
				'role'      => $player['role'],
				'champion'  => $player['champion'],
				'player_id' => $player['player_id'],
				'kills'     => $player['kills'],
				'assists'   => $player['assists'],
				'deaths'    => $player['deaths'],
				'damage'    => $player['damage'],
				'kp'        => round((($player['kills'] + $player['assists']) / $teamKills) * 100),
			];

			if (is_null($playerStats))
				$repository->create($data);
			else
				$repository->update($playerStats->id, $data);
		}
	}
}
