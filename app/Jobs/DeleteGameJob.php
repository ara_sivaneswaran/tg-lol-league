<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class DeleteGameJob
{
    use Dispatchable, SerializesModels;

    private $gameId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($gameId)
    {
        $this->gameId = $gameId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(GameRepositoryInterface $gameRepository, TeamStatsRepositoryInterface $teamStatsRepositoryInterface, GamePlayerStatsRepositoryInterface $gamePlayerStatsRepository, GameBoxScoreRepositoryInterface $gameBoxScoreRepository, MatchRepositoryInterface $matchRepository)
    {
        $game = $gameRepository->find($this->gameId);

        // Update team stats
        $blueTeam = $teamStatsRepositoryInterface->findByTeamSeason($game->blue_team_id, $game->match->season_id)->toArray();
        $redTeam = $teamStatsRepositoryInterface->findByTeamSeason($game->red_team_id, $game->match->season_id)->toArray();

        if($game->winner == $game->blue_team_id){
            $blueTeam['games_won']--;
            $redTeam['games_lost']--;
        }
        else
        {
            $blueTeam['games_lost']--;
            $redTeam['games_won']--;
        }

        $teamStatsRepositoryInterface->update($blueTeam['id'], $blueTeam);
        $teamStatsRepositoryInterface->update($redTeam['id'], $redTeam);

        // Delete game player stats
        $gamePlayerStats = $gamePlayerStatsRepository->findByGame($this->gameId);
        $blueRoster = $gamePlayerStatsRepository->findByGameAndTeam($this->gameId, $game->blue_team_id)->toArray();
        $redRoster = $gamePlayerStatsRepository->findByGameAndTeam($this->gameId, $game->red_team_id)->toArray();

        foreach($gamePlayerStats as $stats)
            $gamePlayerStatsRepository->delete($stats->id);

        // Delete game box score
        $boxScores = $gameBoxScoreRepository->findByGame($this->gameId);

        foreach($boxScores as $boxScore)
            $gameBoxScoreRepository->delete($boxScore->id);

        // Update match stats
        $match = $matchRepository->find($game->match_id)->toArray();
        $match['finished'] = 0;

        if($game->winner == $match['team1_id'])
            $match['team1_score']--;
        else
            $match['team2_score']--;

        $matchRepository->update($game->match_id, $match);

        // Update player stats
        dispatch(new UpdatePlayerStatsJob($game->match->season_id, $game->blue_team_id, $blueRoster));
        dispatch(new UpdatePlayerStatsJob($game->match->season_id, $game->red_team_id, $redRoster));

        // Delete game
        $gameRepository->delete($this->gameId);
    }
}
