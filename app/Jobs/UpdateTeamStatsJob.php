<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateTeamStatsJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $teamId;
	/**
	 * @var
	 */
	private $seasonId;
	/**
	 * @var
	 */
	private $winner;
	/**
	 * @var
	 */
	private $lastGame;
	/**
	 * @var
	 */
	private $finished;
	/**
	 * @var
	 */
	private $winnerChanged;
	
	/**
	 * UpdateTeamStatsJob constructor.
	 *
	 * @param $teamId
	 * @param $seasonId
	 * @param $winner
	 * @param $lastGame
	 * @param $finished
	 * @param $winnerChanged
	 */
	public function __construct ($teamId, $seasonId, $winner, $lastGame, $finished, $winnerChanged)
	{
		$this->teamId        = $teamId;
		$this->seasonId      = $seasonId;
		$this->winner        = $winner;
		$this->lastGame      = $lastGame;
		$this->finished        = $finished;
		$this->winnerChanged = $winnerChanged;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param TeamStatsRepositoryInterface $repository
	 */
	public function handle (TeamStatsRepositoryInterface $repository)
	{
		$teamStat = $repository->findByTeamSeason($this->teamId, $this->seasonId);
		
		if (is_null($teamStat)) {
			$data = [
				'team_id'    => $this->teamId,
				'season_id'  => $this->seasonId,
				'gp'         => 0,
				'wins'       => 0,
				'loss'       => 0,
				'games_won'  => 0,
				'games_lost' => 0
			];
			
		}
		else {
			$data = [
				'team_id'    => $this->teamId,
				'season_id'  => $this->seasonId,
				'gp'         => $teamStat->gp,
				'wins'       => $teamStat->wins,
				'loss'       => $teamStat->loss,
				'games_won'  => $teamStat->games_won,
				'games_lost' => $teamStat->games_lost
			];
		}

		if($this->finished) {
			if ($this->winnerChanged) {
				if ($this->teamId == $this->winner) {
					$data['games_lost'] -= 1;
					
					if ($this->lastGame) {
						$data['gp'] -= 1;
						$data['loss'] -= 1;
					}
				}
				else {
					$data['games_won'] -= 1;
					
					if ($this->lastGame) {
						$data['gp'] -= 1;
						$data['wins'] -= 1;
					}
				}
			}
			else
			{
				if ($this->teamId == $this->winner) {
					$data['games_won'] -= 1;
					
					if ($this->lastGame) {
						$data['gp'] -= 1;
						$data['wins'] -= 1;
					}
				}
				else {
					$data['games_lost'] -= 1;
					
					if ($this->lastGame) {
						$data['gp'] -= 1;
						$data['loss'] -= 1;
					}
				}
			}
		}
		
		if ($this->teamId == $this->winner) {
			$data['games_won'] += 1;
			
			if ($this->lastGame) {
				$data['gp'] += 1;
				$data['wins'] += 1;
			}
		}
		else {
			$data['games_lost'] += 1;
			
			if ($this->lastGame) {
				$data['gp'] += 1;
				$data['loss'] += 1;
			}
		}
		
		
		if (is_null($teamStat))
			$repository->create($data);
		else
			$repository->update($teamStat->id, $data);
	}
}
