<?php

namespace App\Jobs;

use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Repositories\Interfaces\MatchRepositoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class EnterGameResultJob implements ShouldQueue
{
	use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
	/**
	 * @var
	 */
	private $gameId;
	/**
	 * @var
	 */
	private $seasonId;
	/**
	 * @var
	 */
	private $data;
	
	/**
	 * Create a new job instance.
	 *
	 * @param $gameId
	 * @param $seasonId
	 * @param $data
	 */
	public function __construct ($gameId, $seasonId, $data)
	{
		$this->gameId   = $gameId;
		$this->seasonId = $seasonId;
		$this->data     = $data;
	}
	
	/**
	 * Execute the job.
	 *
	 * @param GameRepositoryInterface  $repository
	 * @param MatchRepositoryInterface $matchRepository
	 */
	public function handle (GameRepositoryInterface $repository, MatchRepositoryInterface $matchRepository)
	{
		$update        = false;
		$winnerChanged = false;
		$game          = $repository->find($this->gameId);
		
		if (!is_null($game)){
			$update = true;
			
			if ($game->winner != $this->data['winner'])
				$winnerChanged = true;
		}
		
		$blue_roster = $this->data['blue_team'];
		$red_roster  = $this->data['red_team'];
		
		// Update Match stats
		dispatch(new UpdateMatchStatsJob($game->match, $this->data['winner'], $game->finished, $winnerChanged));
		$match = $matchRepository->find($game->match->id);

		if($match->finished)
			$this->data['last_game'] = true;
		else
			$this->data['last_game'] = false;
		
		// Update Game stats
		$bans = $this->data['bans'];
		dispatch(new UpdateGameStatsJob($this->gameId, $bans, $blue_roster, $red_roster));
		
		// Update Game box score
		dispatch(new UpdateGameBoxScoreJob($this->gameId, $this->data['blue_team_id'], $blue_roster));
		dispatch(new UpdateGameBoxScoreJob($this->gameId, $this->data['red_team_id'], $red_roster));
		
		// Update Team stats
		dispatch(new UpdateTeamStatsJob($this->data['blue_team_id'], $this->seasonId, $this->data['winner'], $this->data['last_game'], $game->finished, $winnerChanged));
		dispatch(new UpdateTeamStatsJob($this->data['red_team_id'], $this->seasonId, $this->data['winner'], $this->data['last_game'], $game->finished, $winnerChanged));
		
		// Update Game player stats
		dispatch(new UpdateGamePlayerStatsJob($game->id, $this->data['blue_team_id'], $this->data['blue_team']));
		dispatch(new UpdateGamePlayerStatsJob($game->id, $this->data['red_team_id'], $this->data['red_team']));
		
		// Update Player stats
		dispatch(new UpdatePlayerStatsJob($this->seasonId, $this->data['blue_team_id'], $this->data['blue_team']));
		dispatch(new UpdatePlayerStatsJob($this->seasonId, $this->data['red_team_id'], $this->data['red_team']));
	}
}
