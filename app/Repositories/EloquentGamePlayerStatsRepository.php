<?php

namespace App\Repositories;

use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use App\GamePlayerStats;

class EloquentGamePlayerStatsRepository implements GamePlayerStatsRepositoryInterface
{
	/**
	 * @var Game
	 */
	private $model;
	
	/**
	 * EloquentGameRepository constructor.
	 *
	 * @param GamePlayerStats $model
	 */
	public function __construct (GamePlayerStats $model)
	{
		$this->model = $model;
	}
	
	
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	public function create ($data)
	{
		$gamePlayerStats = GamePlayerStats::create($data);
		
		return $gamePlayerStats;
	}
	
	public function update ($id, $data)
	{
		$gamePlayerStats = self::find($id);
		
		$gamePlayerStats->update($data);
		
		return $gamePlayerStats;
	}

	/**
	 * Delete game player stats
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$gamePlayerStats = self::find($id);
		
		return $gamePlayerStats->delete();
	}
	
	public function findByGame ($gameId)
	{
		return $this->model->where('game_id', $gameId)->get();
	}
	
	public function findByPlayer ($playerId)
	{
		$games = $this->model->where('player_id', $playerId)->get();
		
		return $games->sortBy(function($game) {
			return $game->game->match->date;
		});
	}
	
	public function findByGameAndTeam ($gameId, $teamId)
	{
		return $this->model->where('game_id', $gameId)->where('team_id', $teamId)->get();
	}
	
	public function findByGameTeamAndPlayer ($gameId, $teamId, $playerId)
	{
		return $this->model->where('game_id', $gameId)->where('team_id', $teamId)->where('player_id', $playerId)->first();
	}
}