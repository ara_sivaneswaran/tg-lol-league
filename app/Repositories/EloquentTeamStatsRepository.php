<?php

namespace App\Repositories;


use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use App\TeamStats;

class EloquentTeamStatsRepository implements TeamStatsRepositoryInterface
{
	/**
	 * @var TeamStat
	 */
	private $model;
	
	/**
	 * EloquentTeamRepository constructor.
	 *
	 * @param TeamStats $model
	 */
	public function __construct (TeamStats $model)
	{
		$this->model = $model;
	}
	
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	public function create ($data)
	{
		$teamStat = TeamStats::create($data);
		
		return $teamStat;
	}
	
	public function update ($id, $data)
	{
		$teamStat = self::find($id);
		
		$teamStat->update($data);
		
		return $teamStat;
	}
	
	public function findByTeam ($teamId)
	{
		return $this->model->where('team_id', $teamId)->get();
	}
	
	public function findByTeamSeason ($teamId, $seasonId)
	{
		return $this->model->where('team_id', $teamId)->where('season_id', $seasonId)->first();
	}
}