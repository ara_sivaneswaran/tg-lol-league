<?php

namespace App\Repositories;

use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Game;

class EloquentGameRepository implements GameRepositoryInterface
{
	/**
	 * @var Game
	 */
	private $model;
	
	/**
	 * EloquentGameRepository constructor.
	 *
	 * @param Game $model
	 */
	public function __construct (Game $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all games value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('match_number', 'desc')->get();
	}
	
	/**
	 * Find the game by id
	 *
	 * @param $id
	 *
	 * @return Game
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new game
	 *
	 * @param $data
	 *
	 * @return Game
	 */
	public function create ($data)
	{
		$game = Game::create($data);
		
		return $game;
	}
	
	/**
	 *  Update a game
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Game
	 */
	public function update ($id, $data)
	{
		$game = self::find($id);
		
		$game->update($data);
		
		return $game;
	}
	
	/**
	 * Delete a game
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$game = self::find($id);
		
		return $game->delete();
	}
	
	public function paginate ($perPage)
	{
		return $this->model->orderBy('match_number', 'desc')->paginate($perPage);
	}
	
	public function byMatch ($matchId)
	{
		return $this->model->where('match_id', $matchId)->orderBy('match_number', 'asc')->get();
	}
	
	public function stats ($gameId)
	{
		$game = self::find($gameId);
		
		return $game->stats;
	}
	
	public function boxScore ($gameId)
	{
		$game = self::find($gameId);
		
		return $game->boxScore;
	}
	
	public function playerStats ($gameId)
	{
		$game = self::find($gameId);
		
		return $game->playerStats;
	}
}