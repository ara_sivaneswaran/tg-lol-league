<?php

namespace App\Repositories;

use App\Repositories\Interfaces\GameStatsRepositoryInterface;
use App\GameStats;

class EloquentGameStatsRepository implements GameStatsRepositoryInterface
{
	/**
	 * @var Game
	 */
	private $model;
	
	/**
	 * EloquentGameRepository constructor.
	 *
	 * @param GameStats $model
	 */
	public function __construct (GameStats $model)
	{
		$this->model = $model;
	}
	
	
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	public function create ($data)
	{
		$gameStats = GameStats::create($data);
		
		return $gameStats;
	}
	
	public function update ($id, $data)
	{
		$gameStats = self::find($id);
		
		$gameStats->update($data);
		
		return $gameStats;
	}
	
	public function findByGame ($gameId)
	{
		return $this->model->where('game_id', $gameId)->first();
	}
}