<?php

namespace App\Repositories;


use App\Repositories\Interfaces\StandingsRepositoryInterface;
use App\Services\StandingsService;

class ServiceStandingsRepository implements StandingsRepositoryInterface {
	/**
	 * @var StandingsService
	 */
	private $service;
	
	/**
	 * ServiceStandingsRepository constructor.
	 *
	 * @param StandingsService $service
	 */
	public function __construct (StandingsService $service)
	{
		$this->service = $service;
	}
	public function bySeason ($seasonId)
	{
		return $this->service->standings($seasonId);
	}
	
	public function bySeasonAndGroup ($seasonId, $groupId)
	{
		return $this->service->groupStandings($seasonId, $groupId);
	}
}