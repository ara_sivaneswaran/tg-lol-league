<?php

namespace App\Repositories;

use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use App\GameBoxScore;

class EloquentGameBoxScoreRepository implements GameBoxScoreRepositoryInterface
{
	/**
	 * @var Game
	 */
	private $model;
	
	/**
	 * EloquentGameRepository constructor.
	 *
	 * @param GameBoxScore $model
	 */
	public function __construct (GameBoxScore $model)
	{
		$this->model = $model;
	}
	
	
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	public function create ($data)
	{
		$gameStats = GameBoxScore::create($data);
		
		return $gameStats;
	}
	
	public function update ($id, $data)
	{
		$gameStats = self::find($id);
		
		$gameStats->update($data);
		
		return $gameStats;
	}

	/**
	 * Delete game box score
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$gameStats = self::find($id);
		
		return $gameStats->delete();
	}
	
	public function findByGame ($gameId)
	{
		return $this->model->where('game_id', $gameId)->get();
	}
	
	public function findByGameAndTeam ($gameId, $teamId)
	{
		return $this->model->where('game_id', $gameId)->where('team_id', $teamId)->first();
	}
}