<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\PlayerStatsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CachePlayerStatsRepository implements PlayerStatsRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGameRepository constructor.
	 *
	 * @param PlayerStatsRepositoryInterface $repository
	 * @param Cache                              $cache
	 */
	public function __construct (PlayerStatsRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function find ($id)
	{
		return $this->cache->tags('playerStats')->remember('playerStats.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('playerStats')->flush();
		$this->cache->tags('players')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('playerStats')->flush();
		$this->cache->tags('players')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function findByPlayer ($playerId)
	{
		return $this->cache->tags('playerStats')->remember('player.' . $playerId . '.stats', 60, function () use ($playerId) {
			return $this->repository->findByPlayer($playerId);
		});
	}
	
	public function findBySeason ($seasonId)
	{
		return $this->cache->tags('playerStats')->remember('player.stats.season.' . $seasonId, 60, function () use ($seasonId) {
			return $this->repository->findBySeason($seasonId);
		});
	}

	public function findBySeasonAndTeam ($seasonId, $teamId)
	{
		return $this->cache->tags('playerStats')->remember('player.stats.season.' . $seasonId . '.team.' . $teamId, 60, function () use ($seasonId, $teamId) {
			return $this->repository->findBySeasonAndTeam($seasonId, $teamId);
		});
	}
	
	public function findByGameTeamAndPlayer ($gameId, $teamId, $playerId)
	{
		return $this->cache->tags('playerStats')->remember('game.' . $gameId . '.team.' . $teamId . '.player.' . $playerId . '.player-stats', 60, function () use ($gameId, $teamId, $playerId) {
			return $this->repository->findByGameTeamAndPlayer($gameId, $teamId, $playerId);
		});
	}
	
	public function findByPlayerAndTeam ($playerId, $teamId)
	{
		return $this->cache->tags('playerStats')->remember('player.' . $playerId . '.team.' . $teamId . '.player-stats', 60, function () use ($playerId, $teamId) {
			return $this->repository->findByGameAndTeam($playerId, $teamId);
		});
	}
	
	public function findByPlayerAndSeason ($playerId, $seasonId)
	{
		return $this->cache->tags('playerStats')->remember('player.' . $playerId . '.season.' . $seasonId . '.player-stats', 60, function () use ($playerId, $seasonId) {
			return $this->repository->findByPlayerAndSeason($playerId, $seasonId);
		});
	}
	
	public function findByPlayerTeamAndSeason ($playerId, $teamId, $seasonId)
	{
		return $this->cache->tags('playerStats')->remember('season.' . $seasonId . '.team.' . $teamId . '.player.' . $playerId . '.player-stats', 60, function () use ($seasonId, $teamId, $playerId) {
			return $this->repository->findByPlayerTeamAndSeason($playerId, $teamId, $seasonId);
		});
	}
}