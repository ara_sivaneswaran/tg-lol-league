<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\GameRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheGameRepository implements GameRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGameRepository constructor.
	 *
	 * @param GameRepositoryInterface $repository
	 * @param Cache                    $cache
	 */
	public function __construct (GameRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('games')->remember('games.all', 60, function () {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('games')->remember('games.ordered.all', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('games')->remember('games.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('games')->flush();
		$this->cache->tags('matches')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('games')->flush();
		$this->cache->tags('matches')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('games')->flush();
		$this->cache->tags('matches')->flush();
		
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('games')->remember('games.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}
	
	public function byMatch ($matchId)
	{
		return $this->cache->tags('games')->remember('games.match.' . $matchId, 60, function () use ($matchId) {
			return $this->repository->byMatch($matchId);
		});
	}
	
	public function stats ($gameId)
	{
		return $this->cache->tags('games')->remember('games.' . $gameId . 'stats', 60, function () use ($gameId) {
			return $this->repository->stats($gameId);
		});
	}
	
	public function boxScore ($gameId)
	{
		return $this->cache->tags('games')->remember('games.' . $gameId . 'boxScore', 60, function () use ($gameId) {
			return $this->repository->boxScore($gameId);
		});
	}
	
	public function playerStats ($gameId)
	{
		return $this->cache->tags('games')->remember('games.' . $gameId . 'playerStats', 60, function () use ($gameId) {
			return $this->repository->playerStats($gameId);
		});
	}
}