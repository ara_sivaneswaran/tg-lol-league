<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheUserRepository implements UserRepositoryInterface {
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheUserRepository constructor.
	 *
	 * @param UserRepositoryInterface $repository
	 * @param Cache                                       $cache
	 */
	public function __construct (UserRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('users')->remember('users.all', 60, function() {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('users')->remember('users.all.ordered', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('users')->remember('users.' . $id, 60, function() use($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('users')->flush();
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('users')->flush();
		return $this->repository->update($id, $data);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('users')->flush();
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('users')->remember('users.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}
}