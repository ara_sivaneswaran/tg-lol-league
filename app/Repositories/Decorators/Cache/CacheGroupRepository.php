<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\GroupRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheGroupRepository implements GroupRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGroupRepository constructor.
	 *
	 * @param GroupRepositoryInterface $repository
	 * @param Cache                     $cache
	 */
	public function __construct (GroupRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('groups')->remember('groups.all', 60, function () {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('groups')->remember('groups.ordered.all', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('groups')->remember('groups.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('groups')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('groups')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('groups')->flush();
		return $this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('groups')->remember('groups.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}

	public function teams ($groupId)
	{
		return $this->cache->tags('groups')->remember('groups.' . $groupId . '.teams', 60, function () use ($groupId) {
			return $this->repository->teams($groupId);
		});
	}}