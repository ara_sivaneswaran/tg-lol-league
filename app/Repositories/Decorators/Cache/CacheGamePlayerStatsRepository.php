<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\GamePlayerStatsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheGamePlayerStatsRepository implements GamePlayerStatsRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGameRepository constructor.
	 *
	 * @param GamePlayerStatsRepositoryInterface $repository
	 * @param Cache                        $cache
	 */
	public function __construct (GamePlayerStatsRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function find ($id)
	{
		return $this->cache->tags('gamePlayerStats')->remember('gamePlayerStats.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('gamePlayerStats')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('gamePlayerStats')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->update($id, $data);
	}

	public function delete ($id)
	{
		$this->cache->tags('gamePlayerStats')->flush();
		$this->cache->tags('games')->flush();
		
		$this->repository->delete($id);
	}
	
	public function findByGame ($gameId)
	{
		return $this->cache->tags('gamePlayerStats')->remember('game.' . $gameId . '.player-stats', 60, function () use ($gameId) {
			return $this->repository->findByGame($gameId);
		});
	}
	
	public function findByPlayer ($playerId)
	{
		return $this->cache->tags('gamePlayerStats')->remember('game.player.' . $playerId . '.player-stats', 60, function () use ($playerId) {
			return $this->repository->findByPlayer($playerId);
		});
	}
	
	public function findByGameAndTeam ($gameId, $teamId)
	{
		return $this->cache->tags('gamePlayerStats')->remember('game.' . $gameId . '.team.' . $teamId . '.player-stats', 60, function () use ($gameId, $teamId) {
			return $this->repository->findByGameAndTeam($gameId, $teamId);
		});
	}
	
	public function findByGameTeamAndPlayer ($gameId, $teamId, $playerId)
	{
		return $this->cache->tags('gamePlayerStats')->remember('game.' . $gameId . '.team.' . $teamId . '.player.' . $playerId . '.player-stats', 60, function () use ($gameId, $teamId, $playerId) {
			return $this->repository->findByGameTeamAndPlayer($gameId, $teamId, $playerId);
		});
	}
}