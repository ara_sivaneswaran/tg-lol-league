<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\StandingsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheStandingsRepository implements StandingsRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheSeasonRepository constructor.
	 *
	 * @param StandingsRepositoryInterface $repository
	 * @param Cache                        $cache
	 */
	public function __construct (StandingsRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function bySeason ($seasonId)
	{
		return $this->cache->tags('standings')->remember('standings.season.' . $seasonId, 60, function () use ($seasonId) {
			return $this->repository->bySeason($seasonId);
		});
	}
	
	public function bySeasonAndGroup ($seasonId, $groupId)
	{
		return $this->cache->tags('standings')->remember('standings.season.' . $seasonId . '.group.' . $groupId, 60, function () use ($seasonId, $groupId) {
			return $this->repository->bySeasonAndGroup($seasonId, $groupId);
		});
	}
}