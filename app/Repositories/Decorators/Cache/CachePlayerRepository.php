<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\PlayerRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CachePlayerRepository implements PlayerRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CachePlayerRepository constructor.
	 *
	 * @param PlayerRepositoryInterface $repository
	 * @param Cache                   $cache
	 */
	public function __construct (PlayerRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('players')->remember('players.all', 60, function () {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('players')->remember('players.ordered.all', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('players')->remember('players.' . $id, 60, function () use($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('players')->flush();
		$this->cache->tags('teams')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('players')->flush();
		$this->cache->tags('teams')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function updatePhoto ($id, $filename)
	{
		$this->cache->tags('players')->flush();
		
		return $this->repository->update($id, $filename);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('players')->flush();
		$this->cache->tags('teams')->flush();
		
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('players')->remember('players.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}
}