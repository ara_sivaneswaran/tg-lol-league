<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\MatchRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheMatchRepository implements MatchRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheMatchRepository constructor.
	 *
	 * @param MatchRepositoryInterface $repository
	 * @param Cache                    $cache
	 */
	public function __construct (MatchRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('matches')->remember('matches.all', 60, function () {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ($order)
	{
		return $this->cache->tags('matches')->remember('matches.ordered.all.order.' . $order, 60, function () use($order) {
			return $this->repository->orderedAll($order);
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('matches')->remember('matches.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('matches')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('matches')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('matches')->flush();
		$this->cache->tags('games')->flush();
		
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage, $order)
	{
		return $this->cache->tags('matches')->remember('matches.all.paginated.order.' . $order, 60, function () use ($perPage, $order) {
			return $this->repository->paginate($perPage, $order);
		});
	}
	
	public function nextFeaturedMatches ()
	{
		return $this->cache->tags('matches')->remember('matches.all.featured', 60, function () {
			return $this->repository->nextFeaturedMatches();
		});
	}
	
	public function nextMatches ()
	{
		return $this->cache->tags('matches')->remember('matches.all.futureAndPresent', 60, function () {
			return $this->repository->nextMatches();
		});
	}
	
	public function firstGame ($matchId)
	{
		return $this->cache->tags('matches')->remember('match.' . $matchId . '.firstGame', 60, function () {
			return $this->repository->firstGame();
		});
	}

	public function byTeam ($teamId)
	{
		return $this->cache->tags('matches')->remember('match.team' . $teamId, 60, function () use($teamId) {
			return $this->repository->byTeam($teamId);
		});
	}

	public function bySeason ($seasonId, $paginate = 0)
	{
		return $this->cache->tags('matches')->remember('match.season' . $seasonId, 60, function () use($seasonId, $paginate) {
			return $this->repository->bySeason($seasonId, $paginate);
		});
	}
}