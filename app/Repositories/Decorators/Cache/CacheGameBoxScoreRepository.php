<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\GameBoxScoreRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheGameBoxScoreRepository implements GameBoxScoreRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGameRepository constructor.
	 *
	 * @param GameBoxScoreRepositoryInterface $repository
	 * @param Cache                        $cache
	 */
	public function __construct (GameBoxScoreRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function find ($id)
	{
		return $this->cache->tags('gameBoxScores')->remember('gameBoxScores.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('gameBoxScores')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('gameBoxScores')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->update($id, $data);
	}

	public function delete ($id)
	{
		$this->cache->tags('gameBoxScores')->flush();
		$this->cache->tags('games')->flush();
		
		$this->repository->delete($id);
	}
	
	public function findByGame ($gameId)
	{
		return $this->cache->tags('gameBoxScores')->remember('game.' . $gameId . '.boxScore', 60, function () use ($gameId) {
			return $this->repository->findByGame($gameId);
		});
	}
	
	public function findByGameAndTeam ($gameId, $teamId)
	{
		return $this->cache->tags('gameBoxScores')->remember('game.' . $gameId . '.team.' . $teamId. '.boxScore', 60, function () use ($gameId, $teamId) {
			return $this->repository->findByGameAndTeam($gameId, $teamId);
		});
	}
}