<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\SeasonRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheSeasonRepository implements SeasonRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheSeasonRepository constructor.
	 *
	 * @param SeasonRepositoryInterface $repository
	 * @param Cache                    $cache
	 */
	public function __construct (SeasonRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('seasons')->remember('seasons.all', 60, function () {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('seasons')->remember('seasons.ordered.all', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('seasons')->remember('seasons.' . $id, 60, function () use($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('seasons')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('seasons')->flush();
		$this->cache->tags('teams')->flush();
		$this->cache->tags('groups')->flush();
		$this->cache->tags('standings')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('seasons')->flush();
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('seasons')->remember('seasons.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}

	public function groups ($seasonId)
	{
		return $this->cache->tags('seasons')->remember('season. ' . $seasonId . '.groups', 60, function () use ($seasonId) {
			return $this->repository->groups($seasonId);
		});
	}
	
	public function currentSeason ()
	{
		return $this->cache->tags('seasons')->remember('season.current', 60, function () {
			return $this->repository->currentSeason();
		});
	}
}