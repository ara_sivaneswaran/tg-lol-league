<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\GameStatsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheGameStatsRepository implements GameStatsRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheGameRepository constructor.
	 *
	 * @param GameStatsRepositoryInterface $repository
	 * @param Cache                        $cache
	 */
	public function __construct (GameStatsRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function find ($id)
	{
		return $this->cache->tags('gameStats')->remember('gameStats.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('gameStats')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('gameStats')->flush();
		$this->cache->tags('games')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function findByGame ($gameId)
	{
		return $this->cache->tags('gameStats')->remember('team.' . $gameId . '.stats', 60, function () use ($gameId) {
			return $this->repository->findByGame($gameId);
		});
	}
}