<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\TeamRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheTeamRepository implements TeamRepositoryInterface {
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheTeamRepository constructor.
	 *
	 * @param TeamRepositoryInterface $repository
	 * @param Cache                                       $cache
	 */
	public function __construct (TeamRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache = $cache;
	}
	
	public function all ()
	{
		return $this->cache->tags('teams')->remember('teams.all', 60, function() {
			return $this->repository->all();
		});
	}
	
	public function orderedAll ()
	{
		return $this->cache->tags('teams')->remember('teams.all.ordered', 60, function () {
			return $this->repository->orderedAll();
		});
	}
	
	public function find ($id)
	{
		return $this->cache->tags('teams')->remember('teams.' . $id, 60, function() use($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('teams')->flush();
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('teams')->flush();
		return $this->repository->update($id, $data);
	}
	
	public function updateLogo ($id, $filename)
	{
		$this->cache->tags('teams')->flush();
		return $this->repository->update($id, $filename);
	}
	
	public function delete ($id)
	{
		$this->cache->tags('teams')->flush();
		$this->repository->delete($id);
	}
	
	public function paginate ($perPage)
	{
		return $this->cache->tags('teams')->remember('teams.all.paginated', 60, function () use ($perPage) {
			return $this->repository->paginate($perPage);
		});
	}

	public function bySeason ($seasonId)
	{
		return $this->cache->tags('teams')->remember('teams.season.' . $seasonId, 60, function () use ($seasonId) {
			return $this->repository->bySeason($seasonId);
		});
	}
	
	public function seasons ($id)
	{
		return $this->cache->tags('teams')->remember('teams.' . $id . '.seasons', 60, function () use ($perPage) {
			return $this->repository->seasonsList();
		});
	}
	
	public function roster ($id)
	{
		return $this->cache->tags('teams')->remember('teams.' . $id . '.roster', 60, function () use ($id) {
			return $this->repository->roster($id);
		});
	}
}