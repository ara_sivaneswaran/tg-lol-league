<?php

namespace App\Repositories\Decorators\Cache;

use App\Repositories\Interfaces\TeamStatsRepositoryInterface;
use Illuminate\Contracts\Cache\Repository as Cache;

class CacheTeamStatsRepository implements TeamStatsRepositoryInterface
{
	/**
	 * @var RepositoryInterface
	 */
	private $repository;
	/**
	 * @var Cache
	 */
	private $cache;
	
	/**
	 * CacheTeamRepository constructor.
	 *
	 * @param TeamStatsRepositoryInterface $repository
	 * @param Cache                   $cache
	 */
	public function __construct (TeamStatsRepositoryInterface $repository, Cache $cache)
	{
		$this->repository = $repository;
		$this->cache      = $cache;
	}
	
	public function find ($id)
	{
		return $this->cache->tags('teamStats')->remember('teamStats.' . $id, 60, function () use ($id) {
			return $this->repository->find($id);
		});
	}
	
	public function create ($data)
	{
		$this->cache->tags('teamStats')->flush();
		$this->cache->tags('teams')->flush();
		$this->cache->tags('standings')->flush();
		
		return $this->repository->create($data);
	}
	
	public function update ($id, $data)
	{
		$this->cache->tags('teamStats')->flush();
		$this->cache->tags('teams')->flush();
		$this->cache->tags('standings')->flush();
		
		return $this->repository->update($id, $data);
	}
	
	public function findByTeam ($teamId)
	{
		return $this->cache->tags('teamStats')->remember('team.' . $teamId . '.stats', 60, function () use ($teamId) {
			return $this->repository->findByTeam($teamId);
		});
	}
	
	public function findByTeamSeason ($teamId, $seasonId)
	{
		return $this->cache->tags('teamStats')->remember('team.' . $teamId . '.season.' .$seasonId .'.stats', 60, function () use ($teamId, $seasonId) {
			return $this->repository->findByTeamSeason($teamId, $seasonId);
		});
	}
}