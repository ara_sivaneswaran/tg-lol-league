<?php

namespace App\Repositories;

use App\Repositories\Interfaces\TeamRepositoryInterface;
use App\Team;

class EloquentTeamRepository implements TeamRepositoryInterface
{
	/**
	 * @var Team
	 */
	private $model;
	
	/**
	 * EloquentTeamRepository constructor.
	 *
	 * @param Team $model
	 */
	public function __construct (Team $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all teams value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('name', 'asc')->get();
	}
	
	/**
	 * Find the team by id
	 *
	 * @param $id
	 *
	 * @return Team
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new team
	 *
	 * @param $data
	 *
	 * @return Team
	 */
	public function create ($data)
	{
		$team = Team::create($data);
		
		return $team;
	}
	
	/**
	 *  Update a team
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Team
	 */
	public function update ($id, $data)
	{
		$team = self::find($id);
		
		$team->update($data);
		
		return $team;
	}
	
	public function updateLogo ($id, $filename)
	{
		$team       = self::find($id);
		$team->logo = $filename;
		$team->save;
		
		return $team;
	}
	
	/**
	 * Delete a team
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$team = self::find($id);
		
		return $team->delete();
	}
	
	public function paginate ($perPage)
	{
		return $this->model->orderBy('name', 'asc')->paginate($perPage);
	}

	public function bySeason($seasonId)
	{
		return $this->model->whereHas('seasons', function ($query) use($seasonId) {
		    $query->where('seasons.id', $seasonId);
		})->get();
	}
	
	public function seasons ($id)
	{
		$seasons = $this->model->seasons()->orderBy('start_date', 'desc')->get();
		
		$array = [];
		
		foreach ($seasons as $season)
			$array[$season->id] = $season->name;
		
		return $array;
	}
	
	public function roster ($id)
	{
		$team = self::find($id);
		
		return $team->players()->orderBy('first_name', 'asc')->orderBy('summoner_name', 'asc')->get();
	}
}