<?php

namespace App\Repositories;

use App\Repositories\Interfaces\MatchRepositoryInterface;
use App\Match;
use Carbon\Carbon;

class EloquentMatchRepository implements MatchRepositoryInterface
{
	/**
	 * @var Match
	 */
	private $model;
	
	/**
	 * EloquentMatchRepository constructor.
	 *
	 * @param Match $model
	 */
	public function __construct (Match $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all matchs value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ($order)
	{
		return $this->model->orderBy('date', $order)->get();
	}
	
	/**
	 * Find the match by id
	 *
	 * @param $id
	 *
	 * @return Match
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new match
	 *
	 * @param $data
	 *
	 * @return Match
	 */
	public function create ($data)
	{
	    if(!array_has($data, 'featured'))
	        $data['featured'] = 0;
	    
		$match = Match::create($data);
		
		return $match;
	}
	
	/**
	 *  Update a match
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Match
	 */
	public function update ($id, $data)
	{
		$match = self::find($id);
		
		$match->update($data);

		if(array_has($data,'claim_credit_gl_account'))
		    $match->featured = 1;
        else
            $match->featured = 0;

        $match->save();
		
		return $match;
	}
	
	/**
	 * Delete a match
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$match = self::find($id);
		
		return $match->delete();
	}
	
	public function paginate ($perPage, $order)
	{
		return $this->model->orderBy('date', $order)->paginate($perPage);
	}
	
	public function nextFeaturedMatches ()
	{
		return $this->model->featured()->futureAndPresent()->orderBy('date', 'asc')->get();
	}
	
	public function nextMatches ()
	{
		return $this->model->future()->orderBy('date', 'asc')->get();
	}
	
	public function firstGame ($matchId)
	{
		$match = self::find($matchId);
		
		return $match->games()->orderBy('match_number', 'asc')->first();
	}

	public function byTeam ($teamId)
	{
		return $this->model->where('team1_id', $teamId)->orWhere('team2_id', $teamId)->orderBy('date', 'asc')->get();
	}

	public function bySeason ($season_id, $paginate)
	{
		if($paginate > 0)
			return $this->model->where('season_id', $season_id)->orderBy('date', 'asc')->paginate($paginate);

		return $this->model->where('season_id', $season_id)->orderBy('date', 'asc')->get();
	}
}