<?php

namespace App\Repositories;

use App\Repositories\Interfaces\SeasonRepositoryInterface;
use App\Season;
use App\Services\SeasonService;
use Carbon\Carbon;

class EloquentSeasonRepository implements SeasonRepositoryInterface
{
	/**
	 * @var Season
	 */
	private $model;
    /**
     * @var SeasonService
     */
    private $seasonService;

    /**
     * EloquentSeasonRepository constructor.
     *
     * @param Season $model
     */
	public function __construct (Season $model)
	{
		$this->model = $model;
        $this->seasonService = new SeasonService();
    }
	
	/**
	 * Get all seasons value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('start_date', 'desc')->get();
	}
	
	/**
	 * Find the season by id
	 *
	 * @param $id
	 *
	 * @return Season
	 */
	public function find ($id)
	{
		return $this->model->where('id', $id)->first();
	}
	
	/**
	 *  Create a new season
	 *
	 * @param $data
	 *
	 * @return Season
	 */
	public function create ($data)
	{
		$season = Season::create($data);
		
		return $season;
	}
	
	/**
	 *  Update a season
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Season
	 */
	public function update ($id, $data)
	{
		$season = self::find($id);
		
		$season->update($data);
		
		return $season;
	}
	
	/**
	 * Delete a season
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$season = self::find($id);
		
		return $season->delete();
	}
	
	public function paginate ($perPage)
	{
		return $this->model->orderBy('start_date', 'desc')->paginate($perPage);
	}

	public function groups ($seasonId)
	{
		$season = self::find($seasonId);

		if($season)
		    return $season->groups()->orderBy('name', 'asc')->get();

		return [];
	}
	
	public function currentSeason ()
	{
		return $this->seasonService->getCurrentSeason();
	}
}