<?php

namespace App\Repositories\Interfaces;


interface UserRepositoryInterface
{
	public function all ();
	
	public function orderedAll ();
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function delete ($id);
}