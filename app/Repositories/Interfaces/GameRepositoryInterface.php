<?php

namespace App\Repositories\Interfaces;


interface GameRepositoryInterface
{
	public function all ();
	
	public function orderedAll ();
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function delete ($id);
	
	public function paginate ($perPage);
	
	public function byMatch ($matchId);
	
	public function stats ($gameId);
	
	public function boxScore ($gameId);
	
	public function playerStats($gameId);
}