<?php

namespace App\Repositories\Interfaces;


interface MatchRepositoryInterface
{
	public function all ();
	
	public function orderedAll ($order);
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function delete ($id);
	
	public function paginate ($perPage, $order);
	
	public function nextFeaturedMatches ();
	
	public function nextMatches();
	
	public function firstGame($matchId);

	public function byTeam($teamId);

	public function bySeason($seasonId, $paginate);
}