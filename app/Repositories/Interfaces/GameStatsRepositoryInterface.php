<?php

namespace App\Repositories\Interfaces;


interface GameStatsRepositoryInterface
{
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function findByGame ($gameId);
}