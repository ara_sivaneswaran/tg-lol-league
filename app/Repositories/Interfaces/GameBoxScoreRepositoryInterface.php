<?php

namespace App\Repositories\Interfaces;


interface GameBoxScoreRepositoryInterface
{
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);

	public function delete($id);
	
	public function findByGame ($gameId);
	
	public function findByGameAndTeam($gameId, $teamId);
}