<?php

namespace App\Repositories\Interfaces;


interface TeamRepositoryInterface
{
	public function all ();
	
	public function orderedAll ();
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function updateLogo($id, $filename);
	
	public function delete ($id);
	
	public function paginate ($perPage);
	
	public function seasons($id);

	public function bySeason($seasonId);
	
	public function roster($id);
}