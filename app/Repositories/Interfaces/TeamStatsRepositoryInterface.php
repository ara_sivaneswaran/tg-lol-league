<?php

namespace App\Repositories\Interfaces;


interface TeamStatsRepositoryInterface
{
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function findByTeam ($teamId);
	
	public function findByTeamSeason($teamId, $seasonId);
}