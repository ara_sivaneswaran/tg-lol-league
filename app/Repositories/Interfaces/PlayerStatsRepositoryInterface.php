<?php

namespace App\Repositories\Interfaces;


interface PlayerStatsRepositoryInterface
{
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function findByPlayer ($playerId);
	
	public function findBySeason ($seasonId);

	public function findBySeasonAndTeam ($seasonId, $teamId);
	
	public function findByPlayerAndTeam ($playerId, $teamId);
	
	public function findByPlayerAndSeason ($playerId, $seasonId);
	
	public function findByPlayerTeamAndSeason ($playerId, $teamId, $seasonId);
}