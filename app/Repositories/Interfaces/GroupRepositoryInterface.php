<?php

namespace App\Repositories\Interfaces;


interface GroupRepositoryInterface
{
	public function all ();
	
	public function orderedAll ();
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);
	
	public function delete ($id);
	
	public function paginate ($perPage);

	public function teams ($groupId);
}