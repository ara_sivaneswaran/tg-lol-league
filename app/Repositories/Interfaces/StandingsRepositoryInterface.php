<?php

namespace App\Repositories\Interfaces;


interface StandingsRepositoryInterface
{
	public function bySeason ($seasonId);
	
	public function bySeasonAndGroup($seasonId, $groupId);
}