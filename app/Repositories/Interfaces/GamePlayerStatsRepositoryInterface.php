<?php

namespace App\Repositories\Interfaces;


interface GamePlayerStatsRepositoryInterface
{
	
	public function find ($id);
	
	public function create ($data);
	
	public function update ($id, $data);

	public function delete($id);
	
	public function findByGame ($gameId);
	
	public function findByPlayer ($playerId);
	
	public function findByGameAndTeam ($gameId, $teamId);
	
	public function findByGameTeamAndPlayer ($gameId, $teamId, $playerId);
}