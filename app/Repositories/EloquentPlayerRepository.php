<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PlayerRepositoryInterface;
use App\Player;

class EloquentPlayerRepository implements PlayerRepositoryInterface
{
	/**
	 * @var Player
	 */
	private $model;
	
	/**
	 * EloquentPlayerRepository constructor.
	 *
	 * @param Player $model
	 */
	public function __construct (Player $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all players value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('first_name', 'asc')->orderBy('summoner_name')->orderBy('last_name', 'asc')->get();
	}
	
	/**
	 * Find the player by id
	 *
	 * @param $id
	 *
	 * @return Player
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new player
	 *
	 * @param $data
	 *
	 * @return Player
	 */
	public function create ($data)
	{
		$player = Player::create($data);
		
		return $player;
	}
	
	/**
	 *  Update a player
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Player
	 */
	public function update ($id, $data)
	{
		$player = self::find($id);
		
		$player->update($data);
		
		return $player;
	}
	
	public function updatePhoto ($id, $filename)
	{
		$player       = self::find($id);
		$player->photo = $filename;
		$player->save;
		
		return $player;
	}
	
	/**
	 * Delete a player
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$player = self::find($id);
		
		return $player->delete();
	}
	
	public function paginate ($perPage)
	{
		return $this->model->orderBy('first_name', 'asc')->orderBy('summoner_name', 'asc')->orderBy('last_name')->paginate($perPage);
	}
}