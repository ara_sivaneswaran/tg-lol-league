<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserRepositoryInterface;
use App\User;

class EloquentUserRepository implements UserRepositoryInterface
{
	/**
	 * @var User
	 */
	private $model;
	
	/**
	 * EloquentUserRepository constructor.
	 *
	 * @param User $model
	 */
	public function __construct (User $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all Users value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('name', 'asc')->get();
	}
	
	/**
	 * Find the user by id
	 *
	 * @param $id
	 *
	 * @return User
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new user
	 *
	 * @param $data
	 *
	 * @return User
	 */
	public function create ($data)
	{
		$user = User::create($data);
		
		return $user;
	}
	
	/**
	 *  Update a User
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return User
	 */
	public function update ($id, $data)
	{
		$user = self::find($id);
		
		$user->update($data);
		
		return $user;
	}
	
	/**
	 * Delete a User
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$user = self::find($id);
		
		return $user->delete();
	}

	public function paginate ($perPage)
	{
		return $this->model->orderBy('name', 'asc')->paginate($perPage);
	}
}