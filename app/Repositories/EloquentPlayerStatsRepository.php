<?php

namespace App\Repositories;

use App\Repositories\Interfaces\PlayerStatsRepositoryInterface;
use App\PlayerStats;
use Illuminate\Support\Facades\DB;

class EloquentPlayerStatsRepository implements PlayerStatsRepositoryInterface
{
	/**
	 * @var Game
	 */
	private $model;
	
	/**
	 * EloquentGameRepository constructor.
	 *
	 * @param PlayerStats $model
	 */
	public function __construct (PlayerStats $model)
	{
		$this->model = $model;
	}
	
	
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	public function create ($data)
	{
		$gamePlayerStats = PlayerStats::create($data);
		
		return $gamePlayerStats;
	}
	
	public function update ($id, $data)
	{
		$gamePlayerStats = self::find($id);
		
		$gamePlayerStats->update($data);
		
		return $gamePlayerStats;
	}
	
	public function findByGame ($gameId)
	{
		return $this->model->where('game_id', $gameId)->get();
	}
	
	public function findBySeason ($seasonId)
	{
		return $this->model->where('season_id', $seasonId)->orderBy(DB::raw('(kills+assists)/deaths'), 'desc')->get();
	}

	public function findBySeasonAndTeam($seasonId, $teamId)
	{
		return $this->model->where('season_id', $seasonId)->where('team_id', $teamId)->orderBy(DB::raw('(kills+assists)/deaths'), 'desc')->get();
	}
	
	public function findByPlayer ($playerId)
	{
		return $this->model->where('player_id', $playerId)->get();
	}
	
	public function findByPlayerAndTeam ($playerId, $teamId)
	{
		return $this->model->where('player_id', $playerId)->where('team_id', $teamId)->get();
	}
	
	public function findByPlayerAndSeason ($playerId, $seasonId)
	{
		return $this->model->where('player_id', $playerId)->where('season_id', $seasonId)->get();
	}
	
	public function findByPlayerTeamAndSeason ($playerId, $teamId, $seasonId)
	{
		return $this->model->where('season_id', $seasonId)->where('team_id', $teamId)->where('player_id', $playerId)->first();
	}
}