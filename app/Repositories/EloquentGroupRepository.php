<?php

namespace App\Repositories;

use App\Repositories\Interfaces\GroupRepositoryInterface;
use App\Group;

class EloquentGroupRepository implements GroupRepositoryInterface
{
	/**
	 * @var Group
	 */
	private $model;
	
	/**
	 * EloquentGroupRepository constructor.
	 *
	 * @param Group $model
	 */
	public function __construct (Group $model)
	{
		$this->model = $model;
	}
	
	/**
	 * Get all groups value
	 */
	public function all ()
	{
		return $this->model->all();
	}
	
	public function orderedAll ()
	{
		return $this->model->orderBy('name', 'asc')->get();
	}
	
	/**
	 * Find the group by id
	 *
	 * @param $id
	 *
	 * @return Group
	 */
	public function find ($id)
	{
		return $this->model->find($id);
	}
	
	/**
	 *  Create a new group
	 *
	 * @param $data
	 *
	 * @return Group
	 */
	public function create ($data)
	{
		$group = Group::create($data);
		
		return $group;
	}
	
	/**
	 *  Update a group
	 *
	 * @param $id
	 * @param $data
	 *
	 * @return Group
	 */
	public function update ($id, $data)
	{
		$group = self::find($id);
		
		$group->update($data);
		
		return $group;
	}
	
	/**
	 * Delete a group
	 *
	 * @param $id
	 *
	 * @return bool|null
	 */
	public function delete ($id)
	{
		$group = self::find($id);
		
		return $group->delete();
	}
	
	public function paginate ($perPage)
	{
		return $this->model->orderBy('name')->paginate($perPage);
	}

	public function teams ($groupId)
	{
		$group = self::find($groupId);

		return $group->teams()->orderBy('name', 'asc')->get();
	}
}