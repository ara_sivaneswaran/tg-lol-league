<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth', 'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
	Route::resource('game', 'GameController', ['except' => 'index']);
	Route::resource('group', 'GroupController');
	Route::get('match/{match}/game/', ['as' => 'match.game.get', 'uses' => 'MatchController@getCreateGame']);
	Route::resource('match', 'MatchController');
	Route::resource('season', 'SeasonController');
	Route::resource('team', 'TeamController');
	Route::resource('player', 'PlayerController');
	Route::resource('user', 'UserController');
	Route::get('/', 'IndexController@dashboard')->name('dashboard');
});

Route::get('/img/{path}', ['as' => 'img', 'uses' => 'ImageController@show'])->where('path', '.*');

Route::get('match/{matchId}', 'HomeController@match')->name('match');
Route::get('player/{playerId}', 'HomeController@player')->name('player');
Route::get('players', 'HomeController@players')->name('players');
Route::get('schedule', 'HomeController@schedule')->name('schedule');
Route::get('standings', 'HomeController@standings')->name('standings');
Route::get('team/{teamId}', 'HomeController@team')->name('team');
Route::get('teams', 'HomeController@teams')->name('teams');
Route::get('/', 'HomeController@index')->name('home');
