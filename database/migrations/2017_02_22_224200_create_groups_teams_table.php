<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groups_teams', function (Blueprint $table) {
            $table->integer('group_id')->unsigned();
	        $table->integer('team_id')->unsigned();
            $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade');
	        $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('groups_teams');
    }
}
