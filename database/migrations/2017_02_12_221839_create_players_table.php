<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('first_name')->nullable();
	        $table->string('last_name')->nullable();
	        $table->string('summoner_name');
	        $table->enum('primary_role', ['top','jng', 'mid', 'adc', 'sup']);
	        $table->enum('secondary_role', ['top', 'jng', 'mid', 'adc', 'sup']);
	        $table->text('photo')->nullable();
            $table->timestamps();
	        $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
