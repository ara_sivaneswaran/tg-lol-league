<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoleToGamePlayerStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_player_stats', function (Blueprint $table) {
	        $table->enum('role', ['top', 'jng', 'mid', 'adc', 'sup'])->after('team_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('game_player_stats', function (Blueprint $table) {
            $table->dropColumn('role');
        });
    }
}
