<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesBoxscores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games_box_scores', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('game_id')->unsigned();
	        $table->integer('team_id')->unsigned();
	        $table->integer('kills');
	        $table->integer('assists');
	        $table->integer('deaths');
	        $table->integer('damage');
            $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games_box_scores');
    }
}
