<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_stats', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('team_id')->unsigned();
	        $table->integer('season_id')->unsigned();
	        $table->integer('gp')->default(0);
	        $table->integer('wins')->default(0);
	        $table->integer('loss')->default(0);
	        $table->integer('games_won')->default(0);
	        $table->integer('games_lost')->default(0);
	        $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_stats');
    }
}
