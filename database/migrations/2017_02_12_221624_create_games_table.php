<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('blue_team_id')->unsigned();
	        $table->integer('red_team_id')->unsigned();
	        $table->integer('match_number');
	        $table->string('duration')->nullable();
	        $table->boolean('finished')->default(false);
	        $table->integer('winner')->unsigned()->nullable();
            $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('blue_team_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('red_team_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('winner')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
