<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamePlayerStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_player_stats', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('game_id')->unsigned();
	        $table->integer('team_id')->unsigned();
	        $table->integer('champion');
	        $table->integer('player_id')->unsigned();
	        $table->integer('kills');
	        $table->integer('assists');
	        $table->integer('deaths');
	        $table->integer('damage');
            $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
	        $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_player_stats');
    }
}
