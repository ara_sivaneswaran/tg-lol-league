<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('season_id')->unsigned();
	        $table->date('date');
	        $table->time('time');
	        $table->integer('team1_id')->unsigned();
	        $table->integer('team2_id')->unsigned();
	        $table->boolean('featured');
	        $table->boolean('finished')->default(false);
	        $table->integer('team1_score')->default(0);
	        $table->integer('team2_score')->default(0);
            $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
	        $table->foreign('team1_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('team2_id')->references('id')->on('teams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
