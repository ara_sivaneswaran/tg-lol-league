<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games_stats', function (Blueprint $table) {
            $table->increments('id');
	        $table->integer('game_id')->unsigned();
	        $table->integer('red_ban_1');
	        $table->integer('red_ban_2');
	        $table->integer('red_ban_3');
	        $table->integer('red_pick_1');
	        $table->integer('red_pick_2');
	        $table->integer('red_pick_3');
	        $table->integer('red_pick_4');
	        $table->integer('red_pick_5');
	        $table->integer('blue_ban_1');
	        $table->integer('blue_ban_2');
	        $table->integer('blue_ban_3');
	        $table->integer('blue_pick_1');
	        $table->integer('blue_pick_2');
	        $table->integer('blue_pick_3');
	        $table->integer('blue_pick_4');
	        $table->integer('blue_pick_5');
            $table->timestamps();
	        $table->softDeletes();
	        
	        $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games_stats');
    }
}
