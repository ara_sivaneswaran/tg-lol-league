<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMoreBanPicksToGameStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('games_stats', function (Blueprint $table) {
	        $table->integer('red_ban_4')->nullable()->after('red_ban_3');
	        $table->integer('red_ban_5')->nullable()->after('red_ban_3');
	        $table->integer('blue_ban_4')->nullable()->after('blue_ban_3');
	        $table->integer('blue_ban_5')->nullable()->after('blue_ban_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games_stats', function (Blueprint $table) {
            $table->dropColumn('red_ban_4');
	        $table->dropColumn('red_ban_5');
	        $table->dropColumn('blue_ban_4');
	        $table->dropColumn('blue_ban_5');
        });
    }
}
