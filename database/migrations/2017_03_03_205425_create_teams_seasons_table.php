<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams_seasons', function (Blueprint $table) {
	        $table->integer('team_id')->unsigned();
	        $table->integer('season_id')->unsigned();
	        $table->timestamps();
	        $table->softDeletes();
	
	        $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
	        $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams_seasons');
    }
}
